# 订单拣选系统 #

为易果生鲜仓库拣选开发的订单拣选系统

### 支持的功能 ###

* 订单的录入和批量导入
* 集合单的创建和管理
* 集合单拣选，包括浏览器端和PDA协同拣选
* 订单的分拣

### 待实现功能 ###

* 拣选人员登陆
* 拣选人员工作量统计
* 与ERP系统的数据集成


### 联系 ###
本系统稳定可靠，可以在此基础上开发自己的拣选系统，如需帮助，请联系我（ssor@qq.com）