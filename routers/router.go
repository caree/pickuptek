package routers

import (
	"github.com/astaxie/beego"
	"pickupTek/controllers"
)

func init() {
	//自动升级需要支持的接口
	beego.Router("/TestAlive", &controllers.MainController{}, "get:TestAlive")
	beego.Router("/exit", &controllers.MainController{}, "get:ExitSelf")
	beego.Router("/Update", &controllers.MainController{}, "get:Update")
	//---------------------------------------------------------------------------------------------

	//
	beego.Router("/NewUpdate", &controllers.MainController{}, "get:NewUpdate") //
	beego.Router("/UpdateNow", &controllers.MainController{}, "get:UpdateNow") //
	beego.Router("/", &controllers.MainController{}, "get:Index")
	beego.Router("/AddOrderIndex", &controllers.MainController{}, "get:AddOrderIndex")
	beego.Router("/AddOrder", &controllers.MainController{}, "get:AddOrder")
	beego.Router("/RemoveOrder", &controllers.MainController{}, "get:RemoveOrder")
	beego.Router("/UploadOrderInfo", &controllers.MainController{}, "post:UploadOrderInfo")
	beego.Router("/OrderInfoList", &controllers.MainController{}, "get:OrderInfoList")
	beego.Router("/OrderListIndex", &controllers.MainController{}, "get:OrderListIndex")
	beego.Router("/NotConfirmedOrderItemsIndex", &controllers.MainController{}, "get:NotConfirmedOrderItemsIndex")
	beego.Router("/MergeOrderIndex", &controllers.MainController{}, "get:MergeOrderIndex")
	beego.Router("/EditMergeOrderIndex", &controllers.MainController{}, "get:EditMergeOrderIndex")
	beego.Router("/MergeOrderManagementIndex", &controllers.MainController{}, "get:MergeOrderManagementIndex")
	beego.Router("/RemoveMergedOrder", &controllers.MainController{}, "get:RemoveMergedOrder")
	beego.Router("/RemoveOrderFromMergedOrder", &controllers.MainController{}, "get:RemoveOrderFromMergedOrder")
	beego.Router("/MergeOrder", &controllers.MainController{}, "get:MergeOrder")
	beego.Router("/OrderDetailIndex", &controllers.MainController{}, "get:OrderDetailIndex")
	beego.Router("/OrderDetail", &controllers.MainController{}, "get:OrderDetail")
	beego.Router("/OrderItemsDetail", &controllers.MainController{}, "get:OrderItemsDetail")
	beego.Router("/ClearCompletedOrders", &controllers.MainController{}, "get:ClearCompletedOrders")
	beego.Router("/NotConfirmedOrderItemList", &controllers.MainController{}, "get:NotConfirmedOrderItemList")
	beego.Router("/ConfirmOrderItemNotIn", &controllers.MainController{}, "get:ConfirmOrderItemNotIn")
	beego.Router("/ConfirmOrderItemIn", &controllers.MainController{}, "get:ConfirmOrderItemIn")

	beego.Router("/GetUncompltedPickupOrdersCount", &controllers.MainController{}, "get:GetUncompltedPickupOrdersCount")
	beego.Router("/SubmitPickupID", &controllers.MainController{}, "get:SubmitPickupID")
	beego.Router("/PickUpIndex", &controllers.MainController{}, "get:PickUpIndex")
	beego.Router("/PickingupOrdersDetailIndex", &controllers.MainController{}, "get:PickingupOrdersDetailIndex")
	beego.Router("/GetPositionsNeedDetail", &controllers.MainController{}, "get:GetPositionsNeedDetail")
	beego.Router("/ClearPosition", &controllers.MainController{}, "get:ClearPosition")
	beego.Router("/CompleteOrderForced", &controllers.MainController{}, "get:CompleteOrderForced")

	beego.Router("/AddProductBarcodeIndex", &controllers.MainController{}, "get:AddProductBarcodeIndex")
	beego.Router("/SearchProductByKeyword", &controllers.MainController{}, "get:SearchProductByKeyword")

	beego.Router("/PickUpMergedOrderIndex", &controllers.MainController{}, "get:PickUpMergedOrderIndex")
	beego.Router("/MergedOrderCompletedIndex", &controllers.MainController{}, "get:MergedOrderCompletedIndex")
	beego.Router("/MergedOrderDetail", &controllers.MainController{}, "get:MergedOrderDetail")
	beego.Router("/GetMergeOrderInfoByOrderID", &controllers.MainController{}, "get:GetMergeOrderInfoByOrderID")
	beego.Router("/SetMergedOrderCompleted", &controllers.MainController{}, "get:SetMergedOrderCompleted")
	beego.Router("/m", &controllers.MainController{}, "get:MergedOrdersIndex")
	beego.Router("/MergedOrders", &controllers.MainController{}, "get:MergedOrders")

	beego.Router("/AddProductIndex", &controllers.MainController{}, "get:AddProductIndex")
	beego.Router("/ProductManagementIndex", &controllers.MainController{}, "get:ProductManagementIndex")
	beego.Router("/ProductNameManagementIndex", &controllers.MainController{}, "get:ProductNameManagementIndex")
	beego.Router("/AddProduct", &controllers.MainController{}, "get:AddProduct")
	beego.Router("/GetProductInfo", &controllers.MainController{}, "get:GetProductInfo")
	beego.Router("/UploadProductInfo", &controllers.MainController{}, "post:UploadProductInfo")
	beego.Router("/RemoveProduct", &controllers.MainController{}, "get:RemoveProduct")
	beego.Router("/ProductList", &controllers.MainController{}, "get:ProductList")
	beego.Router("/ProductNameList", &controllers.MainController{}, "get:ProductNameList")
	beego.Router("/RemoveProductName", &controllers.MainController{}, "get:RemoveProductName")

	// beego.Router("/TrainingProcessesIndex", &controllers.MainController{}, "get:TrainingProcessesIndex")
	// beego.Router("/trainingresult/add", &controllers.MainController{}, "get:AddTrainingResult")
	// beego.Router("/trainingresult/remove", &controllers.MainController{}, "post:RemoveTrainingResults")
	// beego.Router("/trainingresult/list", &controllers.MainController{}, "get:TrainingResults")

}
