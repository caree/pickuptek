package pickupTekLib

import (
	// "bufio"
	// "github.com/astaxie/beego/config"
	// "encoding/json"
	// "errors"
	"fmt"
	// "github.com/codegangsta/cli"
	// "github.com/tealeg/xlsx"
	// "github.com/ungerik/go-dry"
	// "os"
	"strconv"
	// "strings"
	"time"
)

//执行拣选命令
func PickUp(id, shelfID string, chs ...chan *PickupRequestInfo) *PickupInfo {
	DebugInfoF("开始拣选 ID = %s", id)
	chanCallback := make(chan *PickupInfo)
	request := &PickupRequestInfo{chanCallback: chanCallback, ID: id, ShelfID: shelfID}
	var chanPickup chan *PickupRequestInfo
	if len(chs) <= 0 {
		chanPickup = g_chanPickup
	} else {
		chanPickup = chs[0]
	}
	chanPickup <- request
	returnTicker := time.After(time.Second * 2)
	for {
		select {
		case info := <-chanCallback:
			return info
		case <-returnTicker:
			return newPickupInfo(5, nil, nil, "", "操作超时")
		}
	}
}

//将指定拣选墙的某货位的订单清除
func ClearPosition(shelfID, posID string) error {
	return g_shelves.clearPosition(shelfID, posID)
}

//获取拣选墙未完成的订单元素
func GetPositionsNeedDetail(shelfID string) []*PositionNeedDetail {
	list := []*PositionNeedDetail{}
	shelf := g_shelves.find(shelfID)
	if shelf == nil {
		DebugMustF("没有找到编号为 %s 的拣选墙，系统异常", shelfID)
		return list
	}
	for _, pos := range shelf.Positions {
		if pos.occupied() == true {
			order := pos.CurrentOrder
			for _, item := range order.Items {
				if item.CurrentCount < item.CountNeeded {
					list = append(list, newPositionNeedDetail(pos.ID, pos.ShelfID, order.ID, item.ProductID, item.CountNeeded-item.CurrentCount))
				}
			}
		}
	}
	return list
}

//获取拣选墙未完成拣选的订单的数量
func GetUncompltedPickupOrdersCount(shelfID string) int {
	count := 0
	shelf := g_shelves.find(shelfID)
	if shelf == nil {
		DebugMustF("没有找到编号为 %s 的拣选墙，系统异常", shelfID)
		return count
	}

	for _, pos := range shelf.Positions {
		if pos.occupied() == true {
			count += 1
		}
	}
	return count
}

//拣选的逻辑
func doWithPickupEvents(request *PickupRequestInfo,
	orders OrderList, shelves ShelfList,
	mergedOrderList MergedOrderList) *PickupInfo {
	if orders.containsID(request.ID) == true { //这是一个订单编号，可能需要分配货位
		if orders.find(request.ID).completed() == true {
			info := newPickupInfo(其它错误, nil, nil, request.ID, "该订单已拣选完成")
			// request.chanCallback <- info
			return info
		}
		// if pos := Positions.OrderAlreadyLocated(request.ID); pos != nil {
		if pos := shelves.orderAlreadyLocated(request.ID); pos != nil {
			DebugTraceF("订单 %s 已经分配货位", request.ID)
			info := newPickupInfo(无信息改变, nil, nil, "", "")
			// request.chanCallback <- info
			return info
		} else {
			//查看该订单是否可以添加到该拣选墙
			//可以添加到拣选墙的标准是该拣选墙为空，或者已分配的订单与该订单在同一个集合单内
			shelfTemp := shelves.find(request.ShelfID)
			if shelfTemp == nil {
				log := fmt.Sprintf("不存在拣选墙 %s 系统出现异常", request.ShelfID)
				DebugMustF(log)
				info := newPickupInfo(分配货位失败, nil, nil, request.ID, log)
				// request.chanCallback <- info
				return info
			}
			if count, orderIDList := shelfTemp.Positions.occupiedPosCount(); count > 0 { //如果该拣选墙已经被某订单占用了，需要查看占据的订单是否与新传入的订单处于同一个集合单中
				b := mergedOrderList.inSameMergedOrder(request.ID, orderIDList[0])
				if b == false {
					log := fmt.Sprintf("拣选墙 %s 当前的订单与新订单不在同一个集合单内", request.ShelfID)
					DebugMust(log)
					info := newPickupInfo(分配货位失败, nil, nil, request.ID, log)
					// request.chanCallback <- info
					return info
				}
			}
			// if pos, err := Positions.SetOrder(Orders.find(request.ID)); err != nil {
			if pos, err := shelves.setOrder(request.ShelfID, orders.find(request.ID)); err != nil {
				DebugInfoF("分配订单 %s 至 拣选墙 %s 的货位失败，原因：%s ", request.ID, request.ShelfID, err.Error())
				info := newPickupInfo(分配货位失败, nil, nil, request.ID, err.Error())
				// request.chanCallback <- info
				return info
			} else {
				DebugInfoF("分配订单 %s 至 %s 号拣选墙的货位 %d ", request.ID, request.ShelfID, pos.ID)
				info := newPickupInfo(分配货位, pos, nil, request.ID, "")
				// request.chanCallback <- info
				return info
			}
		}
	} else if product := g_products.findByID(request.ID); product != nil { //这是一个产品单号，需要在分配货位的订单中查找需要的订单
		DebugTraceF("接收到产品编码 %s 名称 %s", request.ID, product.Name)
		// if pos, err := Positions.SetProduct(product.Name); err != nil {
		if pos, err := shelves.setProduct(request.ShelfID, product.Name); err != nil {
			DebugInfoF("分配产品到订单时失败，原因:%s", err.Error())
			info := newPickupInfo(分配到订单失败, nil, product, "", err.Error())
			// request.chanCallback <- info
			return info
		} else {
			DebugTraceF("成功将产品 %s 分配给订单 %s 货位 %d", request.ID, pos.CurrentOrder.ID, pos.ID)
			orderID := pos.CurrentOrder.ID
			info := newPickupInfo(分配到订单, pos, product, orderID, "")
			if pos.tryComplete() == true {
				DebugInfoF("货位 %d 的订单拣选完成", pos.ID)
				info = newPickupInfo(订单拣选完成, pos, product, orderID, "")
			}
			// request.chanCallback <- info
			return info
		}
	} else {
		DebugInfoF("发现不能识别的编码 %s", request.ID)
		info := newPickupInfo(其它错误, nil, nil, "", "发现不能识别的编码")
		// request.chanCallback <- info
		return info
	}
}

//初始化拣选墙
func initShelves(shelfCount, posEachShelf int) (shelves ShelfList) {
	for i := 0; i < shelfCount; i++ {
		shelfID := strconv.Itoa(i + 1)
		shelves = append(shelves, newShelf(shelfID, newPositionList(posEachShelf, shelfID)))
	}
	return
}
