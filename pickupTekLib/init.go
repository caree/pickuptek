package pickupTekLib

import (
	"bufio"
	"github.com/ungerik/go-dry"
	// "encoding/json"
	// "errors"
	"fmt"
	// "github.com/HouzuoGuo/tiedot"
	"github.com/codegangsta/cli"
	// "github.com/BurntSushi/toml"
	// "io/ioutil"
	"os"
	"strconv"
	"strings"
	// "time"
	// "archive/zip"
	// "path"
	"github.com/c4milo/unzipit"
	// "path/filepath"
)

const (
	DEFAULT_LOCATION_COUNT = 8
	DEFAULT_SHELF_COUNT    = 4
)

var (
	g_orders         OrderList                   = OrderList{} //订单列表
	g_mergedOrders   MergedOrderList             = MergedOrderList{}
	g_orderItemsTemp OrderItemList               = OrderItemList{} //暂存的订单项列表，其中的内容不确定要导入到正式订单中
	g_shelves        ShelfList                   = ShelfList{}     //拣选墙列表
	g_products       ProductList                 = ProductList{}
	g_chanPickup     chan *PickupRequestInfo     = make(chan *PickupRequestInfo)     //拣选的请求信息
	g_chanMergeOrder chan *MergeOrderRequestInfo = make(chan *MergeOrderRequestInfo) //合并订单成集合单的请求信息
	g_productNames   ProductNameList             = ProductNameList{}                 //数据库中支持的商品名称
	zipfileName                                  = "db.zip"                          //压缩的数据库文件
)

func Init(shelfCount, locationCount int) {
	prepareDB(zipfileName)
	g_products, g_productNames = readDataFromDB()
	g_shelves = initShelves(shelfCount, locationCount)

	excelFileName := "./static/files/订单模板.xlsx"
	orders := extractOrderInfoFromOrderDetail(excelFileName, "")
	g_orders = orders
	go initBusinessEventCenter(g_chanPickup, g_chanMergeOrder, locationCount)
}
func init() {
	// return
	// initConfig()
	// go initCli()

	// if tempList, err := GetAllProductsFromDB(); err == nil {
	// 	Products = tempList
	// 	DebugInfoF("从数据库读取产品信息成功，共包含 %d 个产品信息", len(Products))
	// } else {
	// 	DebugMustF("读取产品信息失败")
	// }
	// if tempList, err := GetAllProductNameFromDB(); err == nil {
	// 	ProductNames = tempList
	// 	DebugInfoF("读取支持的产品名称信息成功, 共 %d 个产品名称", len(ProductNames))
	// } else {
	// 	DebugMustF("读取产品名称信息失败")
	// }
	//---------------------------------------------
	// for _, name := range ProductNames {
	// 	AddProductNameToDB(name)
	// }
	// for _, product := range Products {
	// 	// AddProductToDB(product)
	// }
	// UploadOrderInfoFromFile("订单.xlsx", "2")
	//---------------------------------------------
	// return
	// for i := 0; i < G_conf.ShelfCount; i++ {
	// 	shelfID := strconv.Itoa(i + 1)
	// 	G_Shelves = append(G_Shelves, NewShelf(shelfID, NewPositionList(G_conf.LocationCount, shelfID)))
	// }
	// Positions = NewPositionList(G_locationCount)
}
func readDataFromDB() (ProductList, ProductNameList) {
	productTempList, err := getAllProductsFromDB()
	if err == nil {
		DebugInfoF("从数据库读取产品信息成功，共包含 %d 个产品信息", len(productTempList))
	} else {
		dry.PanicIfErr("读取产品信息失败")
	}
	productNameTempList, err := getAllProductNameFromDB()
	if err == nil {
		DebugInfoF("读取支持的产品名称信息成功, 共 %d 个产品名称", len(productNameTempList))
	} else {
		dry.PanicIfErr("读取产品名称信息失败")
	}
	return productTempList, productNameTempList
}
func initBusinessEventCenter(chPickup chan *PickupRequestInfo, chanMergeOrder chan *MergeOrderRequestInfo,
	maxLocationCount int) {
	for {
		select {
		case request := <-chPickup:
			pi := doWithPickupEvents(request, g_orders, g_shelves, g_mergedOrders)
			request.chanCallback <- pi
		case request := <-chanMergeOrder:
			moi, mergedOrders := doMergeOrderEvents(request, g_orders, maxLocationCount, g_mergedOrders)
			g_mergedOrders = mergedOrders
			request.chanCallback <- moi
		}
	}
}

func initCli() {
	cliApp := cli.NewApp()
	cliApp.Name = "config"
	cliApp.Usage = "设置系统运行参数"
	cliApp.Version = "1.0.1"
	cliApp.Email = "ssor@qq.com"
	cliApp.Commands = []cli.Command{
		{
			Name:        "log",
			ShortName:   "l",
			Usage:       "是否打印log，true为打开，false为关闭",
			Description: "当log太多，而需要与系统交互时可以关闭log的打印",
			Action: func(c *cli.Context) {
				// fmt.Println(fmt.Sprintf("%#v", c.Command))
				// fmt.Println("-----------------------------")
				value := strings.ToLower(c.Args().First())
				if value == "true" {
					fmt.Println("打开log打印")
					G_printLog = true
				} else if value == "false" {
					fmt.Println("关闭log打印")
					G_printLog = false
				} else {
					fmt.Println("参数错误")
				}
			},
		}, {
			Name:        "loglevel",
			ShortName:   "ll",
			Usage:       "设置打印log级别，数字越高，log打印越精细，默认值为3",
			Description: "当需要调试时，可以设置高级别打印",
			Action: func(c *cli.Context) {
				value := strings.ToLower(c.Args().First())
				if level, err := strconv.Atoi(value); err != nil {
					fmt.Println("参数错误")
				} else {
					SetdebugLevel(level)
					fmt.Println("设置log打印级别为" + value)
				}
			},
		}, {
			Name:        "clearOrders",
			ShortName:   "co",
			Usage:       "强制清空当前所有的订单",
			Description: "需要清空订单，但又不想重启系统时使用",
			Action: func(c *cli.Context) {
				//清空货位
				// Positions = NewPositionList(locationCount)
				g_orders = OrderList{}
				fmt.Println("订单清空完成")

			},
		},
	}
	go func() {
		reader := bufio.NewReader(os.Stdin)
		for {
			fmt.Println("等待输入。。。")

			data, _, _ := reader.ReadLine()
			command := string(data)
			cliApp.Run(strings.Split(command, " "))
		}
	}()
	// app.Run(os.Args)
}
func prepareDB(zipfileName string) {
	unzipDB(zipfileName)
	if err := initDB(); err != nil {
		dry.PanicIfErr(fmt.Sprintf("数据库初始化失败：%s", err.Error()))
	}
}
func unzipDB(zipfileName string) {
	// dir := strings.Trim(zipfileName, path.Ext(zipfileName))
	dir := "." + string(os.PathSeparator)
	//处理压缩的数据，解压到当前同名的目录下
	if dry.FileExists(zipfileName) == true {
		if file, err := os.Open(zipfileName); err != nil {
			DebugMustF("打开数据库压缩文件出错：%s", err.Error())
			return
		} else {
			defer func() {
				if file != nil {
					file.Close()
				}
			}()
			DebugTraceF("解压缩数据库文件...")
			if _, errUnzip := unzipit.Unpack(file, dir); errUnzip != nil {
				DebugMustF("解压缩zip文件出错：%s", errUnzip.Error())
				return
			} else {
				file.Close()
				file = nil
				DebugTraceF("正在清理压缩文件 %s...", zipfileName)
				if err := os.Remove(zipfileName); err != nil {
					DebugMustF("清理压缩文件时出错：%s", err.Error())
					return
				}
				DebugTraceF("压缩文件 %s 清理完毕", zipfileName)
			}
		}
	}
}
