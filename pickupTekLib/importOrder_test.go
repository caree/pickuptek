package pickupTekLib

import (
	// "errors"
	"fmt"
	// "github.com/tealeg/xlsx"
	// "strconv"
	// "strings"
	"testing"
	// "time"
)

var (
// Orders OrderList = OrderList{} //订单列表
)

func init() {
}
func Test测试从订单文件导入订单(t *testing.T) {
	excelFileName := "../static/files/订单模板.xlsx"
	orders := extractOrderInfoFromOrderDetail(excelFileName, "")

	if len(orders) != 7 {
		panic(fmt.Sprintf("导入订单数量出错，应为 7 而实际为 %d", len(orders)))
	}
	order7463688 := orders.find("7463688")
	if order7463688 == nil {
		panic(fmt.Sprintf("订单 %s 应该存在", "7463688"))
	}
	if len(order7463688.Items) != 10 {
		panic(fmt.Sprintf("订单 %s 应该含有 10 个订单项，实际为 %d", order7463688.ID, len(order7463688.Items)))
	}

	filter := func(item *OrderItem) int {
		switch item.ProductID {
		case "越南红心火龙果1.2kg", "江西赣南脐橙12个（约200g/个）（北京）":
			return Const_ProductName_NOT_IN
		case "东海带鱼段（300g/包）":
			return Const_ProductName_NOT_SURE
		}
		return Const_ProductName_IN
	}
	ordersConfirmed, orderItemsUncertain := FilterOrderByProductName(orders, filter)

	if len(ordersConfirmed) != 6 {
		panic(fmt.Sprintf("过滤后的订单数量出错，应为 6，而实际为 %d", len(ordersConfirmed)))
	}
	if len(orderItemsUncertain) != 1 {
		panic(fmt.Sprintf("未确定的订单项应该为 1，而实际为 %d", len(orderItemsUncertain)))
	}
}
func Test测试从配送单文件导入订单(t *testing.T) {
	excelFileName := "../static/files/配送单模板.xlsx"
	orders := extractOrderInfoFromDistributionDetail(excelFileName, "")
	if len(orders) != 8 {
		panic(fmt.Sprintf("导入订单数量出错，应为 7 而实际为 %d", len(orders)))
	}
	order7331054 := orders.find("7331054")
	if order7331054 == nil {
		panic(fmt.Sprintf("订单 %s 应该存在", "7331054"))
	}
	if len(order7331054.Items) != 4 {
		panic(fmt.Sprintf("订单 %s 应该含有 4 个订单项，实际为 %d", order7331054.ID, len(order7331054.Items)))
	}
	filter := func(item *OrderItem) int {
		switch item.ProductID {
		case "越南红心火龙果1.2kg", "加拿大北极甜虾熟冻500g（120+/kg）":
			return Const_ProductName_NOT_IN
		case "荷美尔Hormel 经典一口香热狗肠（弹脆）250g（北京）":
			return Const_ProductName_NOT_SURE
		}
		return Const_ProductName_IN
	}
	ordersConfirmed, orderItemsUncertain := FilterOrderByProductName(orders, filter)

	if len(ordersConfirmed) != 7 {
		panic(fmt.Sprintf("过滤后的订单数量出错，应为 7，而实际为 %d", len(ordersConfirmed)))
	}
	if len(orderItemsUncertain) != 2 {
		panic(fmt.Sprintf("未确定的订单项应该为 2，而实际为 %d", len(orderItemsUncertain)))
	}
}
