package pickupTekLib

import (
	"errors"
	"strconv"
	// "time"
	// "fmt"
)

type Shelf struct {
	ID        string
	Positions PositionList
}

func newShelf(id string, list PositionList) *Shelf {
	return &Shelf{
		ID:        id,
		Positions: list,
	}
}

type ShelfList []*Shelf

func (this ShelfList) setOrder(shelfID string, order *Order) (*Position, error) {
	if shelf := this.find(shelfID); shelf == nil {
		DebugMustF("系统异常，没有 %s 号拣选墙", shelfID)
		return nil, errors.New("拣选墙选择错误")
	} else {
		return shelf.Positions.setOrder(order)
	}
}
func (this ShelfList) find(shelfID string) *Shelf {
	for _, shelf := range this {
		if shelf.ID == shelfID {
			return shelf
		}
	}
	return nil
}

func (this ShelfList) orderAlreadyLocated(orderID string) *Position {
	for _, shelf := range this {
		if pos := shelf.Positions.orderAlreadyLocated(orderID); pos != nil {
			return pos
		}
	}
	return nil
}
func (this ShelfList) setProduct(shelfID, productID string) (*Position, error) {
	shelf := this.find(shelfID)
	if shelf == nil {
		DebugMustF("系统异常，没有 %s 号拣选墙", shelfID)
		return nil, errors.New("拣选墙选择错误")
	}
	return shelf.Positions.setProduct(productID)
}

func (this ShelfList) getOrderPos(orderID string) *Position {
	for _, shelf := range this {
		if pos := shelf.Positions.orderAlreadyLocated(orderID); pos != nil {
			return pos
		}
	}
	return nil
}
func (this ShelfList) clearPosition(shelfID, posID string) error {
	shelf := this.find(shelfID)
	if shelf == nil {
		DebugMustF("系统异常，没有 %s 号拣选墙", shelfID)
		return errors.New("拣选墙选择错误")
	}
	for _, pos := range shelf.Positions {
		if strconv.Itoa(pos.ID) == posID {
			pos.clearCurrentOrder()
			return nil
		}
	}
	return errors.New("没有找到该货位")

}
