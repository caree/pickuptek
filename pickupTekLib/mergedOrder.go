package pickupTekLib

import (
	// "errors"
	"fmt"
	// "strconv"
	// "time"
)

type MergedOrder struct {
	ID             string
	Over           bool          //是否拣选完成
	Orders         OrderList     //被合并的订单
	Items          OrderItemList //拣选商品列表
	TotalItemCount int           //集合单中的
	MaxItemCount   int
	// OrderIDList    []string          //被合并的订单的ID
}

func (this *MergedOrder) String() string {
	// DebugInfoF("合并订单  ID：%s ", this.ID)
	// DebugPrintList_Trace(this.Items)
	return fmt.Sprintf("集合单 ID: %s  状态: %t  订单数量：%d  最大数量：%d",
		this.ID, this.Over, this.TotalItemCount, this.MaxItemCount)
}
func (this *MergedOrder) setCompleteState(b bool) {
	this.Over = b
}
func (this *MergedOrder) addOrder(o *Order) error {
	if this.TotalItemCount >= this.MaxItemCount {
		return ERR_MORE_ORDERS_THAN_ALLOWED
	}
	if this.Orders.contains(o) {
		return ERR_NO_ORDER_ALREADY_MERGED
	}
	this.Orders = append(this.Orders, o)
	// this.OrderIDList = append(this.OrderIDList, o.ID)
	// this.Items = this.Items.addRangeWithSameProduct(o.Items)
	// this.Items = this.Orders.allOrderItems().Sum()
	// this.TotalItemCount = this.Items.totalItemCount()
	this.refreshItemsRecord()
	return nil
}
func (this *MergedOrder) removeOrder(orderID string) error {
	if this.Orders.containsID(orderID) {
		this.Orders = this.Orders.remove(orderID)
		this.refreshItemsRecord()
	}
	return nil
}

//重新统计包含的商品统计量
func (this *MergedOrder) refreshItemsRecord() {
	this.Items = this.Orders.allOrderItems().Sum()
	this.TotalItemCount = this.Items.totalItemCount()
}

// func newMergedOrder(id string, items OrderItemList, maxItemCount int) (*MergedOrder, error) {
func newMergedOrder(id string, orders OrderList, maxItemCount int) (*MergedOrder, error) {
	if len(orders) >= maxItemCount {
		return nil, ERR_MORE_ORDERS_THAN_ALLOWED
	}
	mo := &MergedOrder{
		ID:           id,
		Over:         false,
		MaxItemCount: maxItemCount,
		Orders:       orders,
	}
	// mo.Items = mo.Orders.allOrderItems().Sum()
	// mo.TotalItemCount = mo.Items.totalItemCount()
	mo.refreshItemsRecord()
	return mo, nil
}

type MergedOrderList []*MergedOrder

func (this MergedOrderList) InfoList() (list []string) {
	for _, temp := range this {
		list = append(list, temp.String())
	}
	return
}
func (this MergedOrderList) ListName() string {
	return "集合单"
}

// func (this *MergedOrderList) addMergedOrder(mo *MergedOrder) {
// 	this = &append(*this, mo)
// }
func (this MergedOrderList) findContainsOrder(orderID string) *MergedOrder {

	for _, mo := range this {
		if mo.Orders.containsID(orderID) {
			return mo
		}
	}
	return nil
}
func (this MergedOrderList) orderExists(orderID string) (bool, *MergedOrder) {
	if mo := this.findContainsOrder(orderID); mo == nil {
		return false, nil
	} else {
		return true, mo
	}
}
func (this MergedOrderList) find(id string) *MergedOrder {
	for _, mo := range this {
		if mo.ID == id {
			return mo
		}
	}
	return nil
}
func (this MergedOrderList) remove(id string) MergedOrderList {
	for i, mo := range this {
		if mo.ID == id {
			return append(this[:i], this[i+1:]...)
		}
	}
	return this
}
func (this MergedOrderList) inSameMergedOrder(order1, order2 string) bool {
	b1, mo1 := this.orderExists(order1)
	if b1 == false {
		return false
	}
	b2, mo2 := this.orderExists(order2)
	if b2 == false {
		return false
	}
	return mo1.ID == mo2.ID
}
