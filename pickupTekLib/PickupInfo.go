package pickupTekLib

import (
// "bufio"
// "github.com/astaxie/beego/config"
// "encoding/json"
// "errors"
// "fmt"
// "github.com/codegangsta/cli"
// "github.com/tealeg/xlsx"
// "os"
// "strconv"
// "strings"
// "time"
)

type PickupInfo struct {
	Err       string
	Position  *Position
	Product   *Product
	OrderID   string
	StateCode int
}

var (
	无信息改变   int = 0
	分配货位    int = 1
	分配到订单   int = 2
	分配货位失败  int = 3
	分配到订单失败 int = 4
	操作超时    int = 5
	其它错误    int = 6
	订单拣选完成  int = 7
)

func newPickupInfo(code int, pos *Position, product *Product, orderID, err string) *PickupInfo {
	return &PickupInfo{
		Err:       err,
		Position:  pos,
		Product:   product,
		OrderID:   orderID,
		StateCode: code,
	}
}
