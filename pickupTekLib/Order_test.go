package pickupTekLib

import (
	// "errors"
	// "fmt"
	// "github.com/tealeg/xlsx"
	// "strconv"
	// "strings"
	"testing"
	// "time"
)

var (
// Orders OrderList = OrderList{} //订单列表
)

func Test测试重复添加订单项(t *testing.T) {
	list := newOrderItemList(newOrderItem("1", 1, ""), newOrderItem("1", 1, ""))
	if len(list) != 1 {
		t.FailNow()
	}
	if list[0].CountNeeded != 2 {
		t.FailNow()
	}
}
func Test测试重复添加订单(t *testing.T) {
	orders := OrderList{}
	orders = orders.add(newOrder("1", "", "", newOrderItemList(newOrderItem("001", 1, ""))))
	if len(orders) != 1 {
		t.Log(orders)
		t.FailNow()
	}
	orders = orders.add(newOrder("1", "", "", newOrderItemList(newOrderItem("001", 1, ""))))
	if len(orders) != 1 {
		t.Log(orders)
		t.FailNow()
	}
	orders = orders.add(newOrder("2", "", "", newOrderItemList(newOrderItem("001", 1, ""))))
	if len(orders) != 2 {
		t.Log(orders)
		t.FailNow()
	}
}
func Test测试空订单扫描(t *testing.T) {
	orders := OrderList{}
	orders = orders.add(newOrder("o1", "", "", newOrderItemList(newOrderItem("p001", 1, ""))))
	orders = orders.add(newOrder("o2", "", "", newOrderItemList(newOrderItem("p001", 1, ""))))
	orders = orders.add(newOrder("o3", "", "", newOrderItemList(newOrderItem("p001", 1, ""))))

	// positions := NewPositionList(5)

	productID := "p000"
	if b, _ := orders.satisfy(productID); b == true {
		t.Log("错误的产品编号不应该被采用")
		t.FailNow()
	}
	productID = "p001"
	if b, _ := orders.satisfy(productID); b == false {
		t.Log("正确地产品编号应该被采用")
		t.FailNow()
	}
}

func Test查看产品所在的订单(t *testing.T) {
	orders := OrderList{}
	orders = orders.add(newOrder("o1", "", "", newOrderItemList(newOrderItem("p001", 1, ""))))
	orders = orders.add(newOrder("o2", "", "", newOrderItemList(newOrderItem("p002", 2, ""))))
	orders = orders.add(newOrder("o3", "", "", newOrderItemList(newOrderItem("p003", 1, ""))))

	// positions := NewPositionList(5)

	productID := "p002"
	if b, order := orders.satisfy(productID); b == false {
		t.Log("正确地产品编号应该被采用")
		t.FailNow()
	} else {
		if order.ID != "o2" {
			t.Log("查找到的订单不正确")
			t.FailNow()
		}
	}
}
