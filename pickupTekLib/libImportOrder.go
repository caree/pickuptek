package pickupTekLib

import (
	// "bufio"
	// "github.com/astaxie/beego/config"
	// "encoding/json"
	"errors"
	"fmt"
	// "github.com/codegangsta/cli"
	"github.com/tealeg/xlsx"
	"github.com/ungerik/go-dry"
	// "os"
	"strconv"
	"strings"
	// "time"
)

const (
	Const_ProductName_IN       int = 0 //在本库中
	Const_ProductName_NOT_IN   int = 1 //不在本库中
	Const_ProductName_NOT_SURE int = 2 //不确定
)

var (
	Error_ThereIsFiltedOrders error = errors.New("有被过滤的可能非冻库的订单")
	Error_NoSuchProduct             = errors.New("没有找到要删除的产品")
)
var (
	//订单文件模板
	FileTemplateOrderDetail = importedFileTemplate{
		RowHeadderIndex: 0,
		ColHeaderIndex:  0,
		Headers:         []string{"流水号", "司机", "配送时间", "商品编码", "数量", "商品名称"},
		HeaderIndexes:   []int{0, 1, 2, 3, 4, 5},
	}
	//配送单文件模板
	FileTemplateDistributionDetail = importedFileTemplate{
		RowHeadderIndex: 4,
		ColHeaderIndex:  5,
		Headers:         []string{"自由拼序号", "流水号", "司机", "配送时间", "内容", "备注"},
		HeaderIndexes:   []int{1, 4, 5, 6, 7, 10},
	}
)

type importedFileTemplate struct {
	RowHeadderIndex, ColHeaderIndex int      //表头在sheet中的坐标,绝对坐标
	Headers                         []string //表头名称，用于验证
	HeaderIndexes                   []int    //绝对坐标
}

type xlsxContent struct {
	RowIndex  int
	ColValues []string
}

//从文件中导入订单信息和部分商品编码信息
func UploadOrderInfoFromFile(excelFileName, fileType, orderPrefix string) error {
	excelFileName = "temp/" + excelFileName
	// var err error
	var ordersTemp OrderList
	switch fileType {
	case "1": //从配送单中提取订单信息
		DebugInfoF("从配送单中提取订单信息")
		ordersTemp = extractOrderInfoFromDistributionDetail(excelFileName, orderPrefix)
	case "2": //从订单详细情况文件中提取订单信息
		DebugInfoF("从订单中提取订单信息")
		ordersTemp = extractOrderInfoFromOrderDetail(excelFileName, orderPrefix)
	}

	filter := func(item *OrderItem) int {
		return getProductNameStatus(item.ProductID)
	}
	ordersConfirmed, orderItemsUncertain := FilterOrderByProductName(ordersTemp, filter)

	g_orderItemsTemp = g_orderItemsTemp.addRange(orderItemsUncertain)

	addedCount := len(g_orders)
	g_orders = g_orders.addRange(ordersConfirmed)
	addedCount = len(g_orders) - addedCount
	DebugInfoF("成功添加了 %d 个订单", addedCount)
	DebugPrintList_Trace(g_orders)

	if len(g_orderItemsTemp) > 0 {
		return Error_ThereIsFiltedOrders
	}
	return nil
}

//从配送单中提取订单
func extractOrderInfoFromDistributionDetail(excelFileName, orderPrefix string) (orders OrderList) {
	contents := testFileTemplate(excelFileName, FileTemplateDistributionDetail)
	DebugInfo("导入订单信息列表:")
	for _, content := range contents {
		values := content.ColValues
		if len(values) <= 0 {
			continue
		}
		// fmt.Printf("values: %s\r\n", strings.Join(values, ","))
		_orderID := values[1] //流水号
		_content := values[4]
		if len(_orderID) <= 0 {
			continue
		}
		_orderID = orderPrefix + _orderID //易果系统会自动添加 前缀 作为识别码
		if len(_content) <= 0 {
			continue
		}
		items := splitOrderItems(_content, _orderID)
		// itemsFilted = itemsFilted.addRange(_itemsFilted)
		if len(items) <= 0 {
			DebugSys("订单里没有订购信息，出现解析异常")
			continue
		}
		if orderTemp := orders.find(_orderID); orderTemp == nil {
			orders = orders.add(newOrder(_orderID, "", "", items))
		}
	}
	return
	/*	var err error
		var xlFile *xlsx.File
		if xlFile, err = xlsx.OpenFile(excelFileName); err != nil {
			DebugMust(err.Error())
			return nil, err
		}
		//检查格式
		DebugTraceF("文件中共有 %d 个 sheet", len(xlFile.Sheets))

		if len(xlFile.Sheets) <= 0 {
			return nil, errors.New("文件中没有数据")
		}
		firstSheet := xlFile.Sheets[0]

		// firstSheet := sheet
		DebugTrace(fmt.Sprintf("文件最大列是 %d  最大行是 %d", firstSheet.MaxCol, firstSheet.MaxRow))
		if len(firstSheet.Rows) <= 5 || len(firstSheet.Cols) < 14 {
			return nil, errors.New("文件格式错误")
		}
		rows := firstSheet.Rows

		//****************************************************************************************
		// 数据从行4开始，包含内容的列 1 自由拼序号 4 流水号 5 司机 6 配送时间 7 内容  10 备注
		//****************************************************************************************
		testColName := func(colName []string, colIndex []int, row *xlsx.Row) error {
			length := len(colName)
			for i := 0; i < length; i++ {
				if strings.Trim(row.Cells[colIndex[i]].Value, " ") != colName[i] {
					return errors.New(fmt.Sprintf("文件格式错误, 第 %d 列应该是 %s", i+1, colName[i]))
				}
			}
			return nil
		}
		if err := testColName([]string{"自由拼序号", "流水号", "司机", "配送时间", "内容", "备注"},
			[]int{1, 4, 5, 6, 7, 10},
			rows[4]); err != nil {
			return nil, err
		}

		DebugInfo("导入订单信息列表:")
		ordersTemp := OrderList{}
		itemsFilted := OrderItemList{}
		rows = rows[5:]
		for _, row := range rows {
			// DebugTrace(fmt.Sprintf("第 %d 行 含有 %d 列", i, len(row.Cells)) + GetFileLocation())
			if len(row.Cells) < 13 {
				continue
			}
			// str := ""
			// for i := 0; i < maxCol; i++ {
			// 	str = str + "    " + row.Cells[i].String()
			// }
			// DebugInfo(str + GetFileLocation())

			// _sequenceID := strings.Trim(row.Cells[1].Value, " ")//自由拼序号
			_orderID := strings.Trim(row.Cells[4].Value, " ") //流水号
			_content := strings.Trim(row.Cells[7].Value, " ")

			DebugInfoF("%s   %s", _orderID, _content)
			// _orderID = G_OrderPrefix + _orderID //易果系统会自动添加 前缀 作为识别码
			_orderID = G_conf.OrderPrefix + _orderID //易果系统会自动添加 前缀 作为识别码
			if len(_content) <= 0 {
				continue
			}
			items, _itemsFilted := splitOrderItems(_content, _orderID)
			itemsFilted = itemsFilted.addRange(_itemsFilted)
			if len(items) <= 0 {
				DebugSys("订单里没有订购信息，出现解析异常")
				continue
			}
			if orderTemp := ordersTemp.Find(_orderID); orderTemp == nil {
				ordersTemp = ordersTemp.Add(NewOrder(_orderID, "", "", items))
			} else {
				// orderTemp.AddOrderItems(items)
				//每行只有一个订单，如果之前已经有该订单，说明是订单重复导入
			}
		}
		if len(itemsFilted) > 0 {
			OrderItemsTemp = OrderItemsTemp.addRange(itemsFilted)
			return ordersTemp, Error_ThereIsFiltedOrders
		} else {
			return ordersTemp, nil
		}
	*/
}

//将配送单里面的内容拆分成订单
func splitOrderItems(content, orderID string) (list OrderItemList) {
	// orderItemsFilted := OrderItemList{}
	// list := OrderItemList{}
	if strings.Contains(content, "自由拼：") == true {
		content = strings.Replace(content, "自由拼：", "", -1)
		content = strings.Replace(content, "，", ",", -1)
		content = strings.Replace(content, "：", ":", -1)
		nameWithCountList := strings.Split(content, ",")
		for _, combined := range nameWithCountList {
			DebugTraceF("将要解析订单项：%s", combined)
			countFlagIndex := strings.Index(combined, "数量:")
			productName := strings.Trim(combined[:countFlagIndex], " ")
			strCount := strings.Trim(combined[countFlagIndex+7:], " ")
			count, err := strconv.ParseFloat(strCount, 32)
			if err != nil {
				dry.PanicIfErr(fmt.Sprintf("解析订单数量时出错：%s %s  %s", err.Error(), productName, strCount))
			}
			DebugTraceF("解析数量: %s , 结果为 ： %f", strCount, count)
			DebugInfoF(fmt.Sprintf("解析出订单项：名称=> %s 数量 => %d", productName, int(count)))
			newOrderItem := newOrderItem(productName, int(count), orderID)
			list = append(list, newOrderItem)
			/*			status := getProductNameStatus(productName)
						switch status {
						case Const_ProductName_IN:
							list = append(list, newOrderItem)

						case Const_ProductName_NOT_IN:
							DebugInfoF("%s 已经被过滤", productName)

						case Const_ProductName_NOT_SURE:
							DebugInfoF("%s 无法确定，添加到待确定列表中", productName)
							orderItemsFilted = append(orderItemsFilted, newOrderItem)
						}

						if ProductShouldBeFiltered(productName) == true {
							DebugInfo(productName + " 已经被过滤")
							orderItemsFilted = append(orderItemsFilted, newOrderItem)
						} else {
							list = append(list, newOrderItem)
						}*/

		}
	}
	return
	// return list, orderItemsFilted
}

func testFileTemplate(excelFileName string, tpl importedFileTemplate) (contents []xlsxContent) {
	// var xlFile *xlsx.File
	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		dry.PanicIfErr("导入文件", err)
	}
	//检查格式
	DebugTraceF("文件中共有 %d 个 sheet", len(xlFile.Sheets))

	if len(xlFile.Sheets) <= 0 {
		dry.PanicIfErr("文件中没有数据")
	}
	firstSheet := xlFile.Sheets[0]

	if len(firstSheet.Rows) <= tpl.RowHeadderIndex || len(firstSheet.Cols) < tpl.ColHeaderIndex {
		dry.PanicIfErr("文件中没有数据")
	}
	headerRow := firstSheet.Rows[tpl.RowHeadderIndex]
	testColName := func(colName []string, colIndexes []int, row *xlsx.Row) error {
		length := len(colName)
		for i := 0; i < length; i++ {
			value := row.Cells[colIndexes[i]].Value
			if strings.Trim(value, " ") != colName[i] {
				return errors.New(fmt.Sprintf("文件格式错误, 第 %d 列应该为 (%s)，实际为 (%s)", colIndexes[i], colName[i], value))
				// return errors.New(fmt.Sprintf("文件格式错误, 各列分别是 %s", strings.Join(colName, ",")))
			}
		}
		return nil
	}
	if err := testColName(tpl.Headers, tpl.HeaderIndexes, headerRow); err != nil {
		dry.PanicIfErr(err)
	}
	rowsData := firstSheet.Rows[tpl.RowHeadderIndex+1:]
	for rowIndex, row := range rowsData {
		content := xlsxContent{
			RowIndex:  rowIndex,
			ColValues: []string{},
		}
		for i := 0; i < len(tpl.HeaderIndexes); i++ {
			if len(row.Cells) < len(tpl.HeaderIndexes) {
				continue
			}
			// fmt.Printf("共 %d 列，当前序号 %d\r\n", len(row.Cells), tpl.HeaderIndexes[i])
			value := row.Cells[tpl.HeaderIndexes[i]].Value
			content.ColValues = append(content.ColValues, strings.Trim(value, " "))
		}
		contents = append(contents, content)
	}
	return
}

//从订单中提取订单，不对订单做任何处理
func extractOrderInfoFromOrderDetail(excelFileName, orderPrefix string) (orders OrderList) {
	contents := testFileTemplate(excelFileName, FileTemplateOrderDetail)
	DebugInfo("导入订单信息列表:")
	for _, content := range contents {
		values := content.ColValues
		_orderID := values[0]
		_deliver := values[1]
		_delivingTime := values[2]
		_productBarcode := values[3]
		_countStr := values[4]
		_productName := values[5]

		// _orderID = G_conf.OrderPrefix + _orderID //易果系统会自动添加 前缀 作为识别码
		_orderID = orderPrefix + _orderID //易果系统会自动添加 前缀 作为识别码
		_count, err := strconv.Atoi(_countStr)
		if err != nil {
			DebugMustF("转换订单（%s）中商品（%s）数量时出错：%s", _orderID, _productBarcode, err.Error())
			continue
		}
		newOrderItem := newOrderItem(_productName, _count, _orderID)
		if orderTemp := orders.find(_orderID); orderTemp == nil {
			orders = orders.add(newOrder(_orderID, _deliver, _delivingTime, OrderItemList{newOrderItem}))
		} else {
			orderTemp.addOrderItem(newOrderItem)
		}
	}
	return
	/*	var err error
			var xlFile *xlsx.File
			if xlFile, err = xlsx.OpenFile(excelFileName); err != nil {
				DebugMust(err.Error())
				return nil, err
			}
			//检查格式
			DebugTraceF("文件中共有 %d 个 sheet", len(xlFile.Sheets))

			if len(xlFile.Sheets) <= 0 {
				return nil, errors.New("文件中没有数据")
			}
			firstSheet := xlFile.Sheets[0]

			// firstSheet := sheet
			if len(firstSheet.Rows) <= 1 || len(firstSheet.Cols) < 6 {
				return nil, errors.New("文件中没有数据")
			}
			rows := firstSheet.Rows
			firstRow := rows[0]
			rowHeader := fmt.Sprintf("%s(%s)    %s(%s)    %s(%s)    %s(%s)    %s(%s)",
				firstRow.Cells[0].String(), firstRow.Cells[0].GetNumberFormat(),
				firstRow.Cells[1].String(), firstRow.Cells[1].GetNumberFormat(),
				firstRow.Cells[2].Value, firstRow.Cells[2].GetNumberFormat(),
				firstRow.Cells[3].Value, firstRow.Cells[3].GetNumberFormat(),
				firstRow.Cells[4].String(), firstRow.Cells[4].GetNumberFormat())

			DebugInfo(rowHeader)
			testColName := func(colName []string, row *xlsx.Row) error {
				length := len(colName)
				for i := 0; i < length; i++ {
					if strings.Trim(row.Cells[i].Value, " ") != colName[i] {
						return errors.New(fmt.Sprintf("文件格式错误, 第 %d 列应该是 %s", i+1, colName[i]))
					}
				}
				return nil
			}
			if err := testColName([]string{"流水号", "司机", "配送时间", "商品编码", "数量", "商品名称"}, firstRow); err != nil {
				return nil, err
			}
		ordersTemp := OrderList{}
		maxCol := 6
		rows = rows[1:]
		for _, row := range rows {

			str := ""
			for i := 0; i < maxCol; i++ {
				str = str + "    " + row.Cells[i].String()
			}

			_orderID := strings.Trim(row.Cells[0].Value, " ")
			_deliver := strings.Trim(row.Cells[1].Value, " ")
			_delivingTime := strings.Trim(row.Cells[2].String(), " ")
			_productBarcode := strings.Trim(row.Cells[3].Value, " ")
			_countStr := strings.Trim(row.Cells[4].Value, " ")
			_productName := strings.Trim(row.Cells[5].Value, " ")

			_orderID = G_conf.OrderPrefix + _orderID //易果系统会自动添加 前缀 作为识别码
			// _orderID = G_OrderPrefix + _orderID //易果系统会自动添加 前缀 作为识别码
			_count, err := strconv.Atoi(_countStr)
			if err != nil {
				DebugMustF("转换订单（%s）中商品（%s）数量时出错：%s", _orderID, _productBarcode, err.Error())
				continue
			}
			newOrderItem := NewOrderItem(_productName, _count, _orderID)
			if orderTemp := ordersTemp.Find(_orderID); orderTemp == nil {
				ordersTemp = ordersTemp.Add(NewOrder(_orderID, _deliver, _delivingTime, OrderItemList{newOrderItem}))
			} else {
				orderTemp.AddOrderItem(newOrderItem)
			}
		}
		return ordersTemp, nil
	*/
}
func FilterOrderByProductName(orders OrderList, filterFunc func(*OrderItem) int) (listConfirmed OrderList, listUncertain OrderItemList) {
	for _, order := range orders {
		itemsTemp := OrderItemList{}
		for _, item := range order.Items {
			status := filterFunc(item)
			switch status {
			case Const_ProductName_IN:
				itemsTemp = append(itemsTemp, item)
			case Const_ProductName_NOT_IN:
				DebugInfoF("%s 已经被过滤", item.ProductID)
			case Const_ProductName_NOT_SURE:
				DebugInfoF("%s 无法确定，添加到待确定列表中", item.ProductID)
				listUncertain = append(listUncertain, item)
			}
		}
		order.Items = itemsTemp
		if len(order.Items) > 0 {
			listConfirmed = append(listConfirmed, order)
		}
	}
	return
}
