package pickupTekLib

import (
// "bufio"
// "github.com/astaxie/beego/config"
// "encoding/json"
// "errors"
// "fmt"
// "github.com/codegangsta/cli"
// "github.com/tealeg/xlsx"
// "github.com/ungerik/go-dry"
// "os"
// "strconv"
// "strings"
// "time"
)

//货位分配的尚未拣选完成的订单的需要的商品信息
type PositionNeedDetail struct {
	PositionID  int
	ShelfID     string
	OrderID     string
	ProductName string
	CountNeeded int
}

func newPositionNeedDetail(posID int, shelfID, orderID, productName string, count int) *PositionNeedDetail {
	return &PositionNeedDetail{
		PositionID:  posID,
		OrderID:     orderID,
		ProductName: productName,
		CountNeeded: count,
	}
}
