package pickupTekLib

import (
// "bufio"
// "github.com/astaxie/beego/config"
// "encoding/json"
// "errors"
// "fmt"
// "github.com/codegangsta/cli"
// "github.com/tealeg/xlsx"
// "os"
// "strconv"
// "strings"
// "time"
)

type MergeOrderInfo struct {
	// Err         string
	Err         error
	OrderID     string
	MergedOrder *MergedOrder
}

func newMergeOrderInfo(orderID string, err error, mo *MergedOrder) *MergeOrderInfo {
	return &MergeOrderInfo{
		OrderID:     orderID,
		Err:         err,
		MergedOrder: mo,
	}
}
