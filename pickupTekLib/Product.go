package pickupTekLib

import (
	// "errors"
	// "time"
	// "fmt"
	"strings"
)

const (
	ProductType_Frozen     = 0
	ProductType_Non_Frozen = 1
)

type ProductName struct {
	Name        string
	ProductType int //所属品类，0，冻品 1，非冻品
	docID       int //存储在数据库中的ID
}

func newProductName(name string, productType int) *ProductName {
	return &ProductName{
		Name:        name,
		ProductType: productType,
	}
}
func newProductNameWithDocID(name string, productType, docID int) *ProductName {
	return &ProductName{
		Name:        name,
		ProductType: productType,
		docID:       docID,
	}
}
func (this *ProductName) toMapInterface() map[string]interface{} {
	return map[string]interface{}{
		"Name":        this.Name,
		"ProductType": this.ProductType,
	}
}

type ProductNameList []*ProductName

func (this ProductNameList) toStringList() []string {
	list := []string{}
	for _, temp := range this {
		list = append(list, temp.Name)
	}
	return list
}
func (this ProductNameList) find(name string) *ProductName {
	for _, temp := range this {
		if temp.Name == name {
			return temp
		}
	}
	return nil
}

func (this ProductNameList) contains(name string) bool {
	if this.find(name) == nil {
		return false
	}
	return true
}
func (this ProductNameList) searchKeyword(keyword string) []string {
	list := ProductNameList{}
	for _, temp := range this {
		if temp.ProductType == ProductType_Frozen && strings.Contains(temp.Name, keyword) == true {
			// DebugTrace(fmt.Sprintf("%s 含有关键字 %s ", temp.Name, keyword) + GetFileLocation())
			if list.contains(temp.Name) == false {
				list = append(list, newProductName(temp.Name, ProductType_Frozen))
			}
		} else {
			// DebugTrace(fmt.Sprintf("%s 不含有关键字 %s ", temp.Name, keyword) + GetFileLocation())

		}
	}
	return list.toStringList()
}

// type ProductNameList []string

// func (this ProductNameList) Contains(name string) bool {
// 	for _, temp := range this {
// 		if temp.Name == name {
// 			return true
// 		}
// 	}
// 	return false
// }
type Product struct {
	Barcode string
	Name    string
	docID   int //存储在数据库中的ID
}

func (this *Product) toMapInterface() map[string]interface{} {
	return map[string]interface{}{
		"Barcode": this.Barcode,
		"Name":    this.Name,
	}
}
func newProduct(id, name string) *Product {
	return &Product{
		Barcode: id,
		Name:    name,
	}
}
func newProductWithDocID(id, name string, docID int) *Product {
	return &Product{
		Barcode: id,
		Name:    name,
		docID:   docID,
	}
}

type ProductList []*Product

func (this ProductList) add(product *Product) ProductList {
	if this.contains(product.Barcode, product.Name) {
		return this
	}
	return append(this, product)
}

func (this ProductList) getProductBarcodeListByName(name string) []string {
	temp := []string{}
	for _, product := range this {
		if name == product.Name {
			temp = append(temp, product.Barcode)
		}
	}
	return temp
}
func (this ProductList) removeByName(name string) ProductList {
	temp := ProductList{}
	for _, product := range this {
		if name == product.Name {
			DebugInfoF("查找到了与要删除的产品名称相同的产品信息 %s ", name)
		} else {
			temp = append(temp, product)
		}
	}
	return temp
}
func (this ProductList) remove(id string) ProductList {
	temp := ProductList{}
	for _, product := range this {
		if id == product.Barcode {
			DebugInfoF("查找到了与要删除的产品编号相同的产品信息 %s %s", id, product.Name)
		} else {
			temp = append(temp, product)
		}
	}
	return temp
}
func (this ProductList) findByID(id string) *Product {
	for _, product := range this {
		if product.Barcode == id {
			return product
		}
	}
	return nil
}
func (this ProductList) find(id, name string) *Product {
	for _, product := range this {
		if product.Name == name && product.Barcode == id {
			return product
		}
	}
	return nil
}
func (this ProductList) contains(id, name string) bool {
	if temp := this.find(id, name); temp == nil {
		return false
	} else {
		return true
	}
}

// func (this ProductList) SearchKeyword(keyword string) []string {
// 	list := []string{}
// 	for _, product := range this {
// 		if strings.Contains(product.Name, keyword) == true {
// 			if list.Contains(product.Name) == false {
// 				list = append(list, product.Name)
// 			}
// 		} else {
// 			DebugTrace(fmt.Sprintf("名称 %s 中不含有关键字 %s", product.Name, keyword) + GetFileLocation())
// 		}
// 	}
// 	return list
// }
