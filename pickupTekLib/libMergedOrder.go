package pickupTekLib

import (
	// "bufio"
	// "github.com/astaxie/beego/config"
	// "encoding/json"
	// "errors"
	"fmt"
	// "github.com/codegangsta/cli"
	// "github.com/tealeg/xlsx"
	// "github.com/ungerik/go-dry"
	// "os"
	// "strconv"
	// "strings"
	"time"
)

//处理创建集合单逻辑
func doMergeOrderEvents(request *MergeOrderRequestInfo,
	orders OrderList, maxCount int,
	mergedOrderList MergedOrderList) (*MergeOrderInfo, MergedOrderList) {
	var o *Order
	if o = orders.find(request.OrderID); o == nil {
		DebugMustF("创建集合单时，没找到订单 (%s)", request.OrderID)
		return newMergeOrderInfo(request.OrderID, ERR_NO_SUCH_ORDER, nil), mergedOrderList
	}
	if b, _ := mergedOrderList.orderExists(request.OrderID); b == true {
		log := fmt.Sprintf("订单 %s 已经被合并过", request.OrderID)
		DebugSysF(log)
		return newMergeOrderInfo(request.OrderID, ERR_NO_ORDER_ALREADY_MERGED, nil), mergedOrderList
	}
	DebugInfoF("订单 %s 可以被合并", request.OrderID)

	if len(request.MergedOrderID) <= 0 {
		//提交第一个订单
		newOrders := OrderList{o}
		mo, err := newOrders.merge(maxCount)
		if err != nil {
			DebugMustF("创建集合单出错：%s", err.Error())
			return newMergeOrderInfo(request.OrderID, err, nil), mergedOrderList
		}
		mergedOrderList = append(mergedOrderList, mo)
		DebugTraceF("创建集合单: %s", mo.String())
		return newMergeOrderInfo(request.OrderID, nil, mo), mergedOrderList
	} else {
		//查找之前的集合单，将新订单加进去
		if mo := mergedOrderList.find(request.MergedOrderID); mo == nil {
			DebugMustF("集合单 %s 不存在", request.MergedOrderID)
			return newMergeOrderInfo(request.OrderID, ERR_NO_SUCH_MERGEDORDER, nil), mergedOrderList
		} else {
			err := mo.addOrder(o)
			if err != nil {
				DebugMustF("向集合单(%s)添加订单(%s)出错：%s", request.MergedOrderID, request.OrderID, err)
				return newMergeOrderInfo(request.OrderID, err, nil), mergedOrderList
			}
			DebugTraceF("创建集合单: %s", mo.String())
			return newMergeOrderInfo(request.OrderID, nil, mo), mergedOrderList
		}
	}
	// return nil, orders, mergedOrderList
}

/*func doMergeOrderEvents(request *MergeOrderRequestInfo, orders OrderList, maxCount int, mergedOrderList MergedOrderList) {
	var o *Order
	if o = orders.find(request.OrderID); o == nil {
		request.chanCallback <- newMergeOrderInfo(request.OrderID, "该订单不存在", nil)
		return
	}
	if b, _ := mergedOrderList.orderExists(request.OrderID); b == true {
		log := fmt.Sprintf("订单 %s 已经被合并过", request.OrderID)
		DebugSysF(log)
		request.chanCallback <- newMergeOrderInfo(request.OrderID, log, nil)
		return
	}
	DebugInfoF("订单 %s 可以被合并", request.OrderID)

	if len(request.MergedOrderID) <= 0 {
		//提交第一个订单
		newOrders := OrderList{o}
		mo, err := newOrders.merge(maxCount)
		if err != nil {
			request.chanCallback <- newMergeOrderInfo(request.OrderID, err.Error(), nil)
			return
		}
		mergedOrderList = append(mergedOrderList, mo)
		DebugTraceF("创建集合单: %s", mo.String())
		request.chanCallback <- newMergeOrderInfo(request.OrderID, "", mo)
		return
	} else {
		//查找之前的集合单，将新订单加进去
		if mo := mergedOrderList.find(request.MergedOrderID); mo == nil {
			request.chanCallback <- newMergeOrderInfo(request.OrderID, "找不到该集合单单号", nil)
			return
		} else {
			err := mo.addOrder(o)
			if err != nil {
				request.chanCallback <- newMergeOrderInfo(request.OrderID, err.Error(), nil)
				return
			}
			DebugTraceF("创建集合单: %s", mo.String())
			request.chanCallback <- newMergeOrderInfo(request.OrderID, "", mo)
			return
		}
	}
}*/

func removeMergedOrder(mergedOrderID string, mergedOrderList MergedOrderList) (MergedOrderList, error) {
	if mo := mergedOrderList.find(mergedOrderID); mo == nil {
		return mergedOrderList, ERR_NO_SUCH_MERGEDORDER
	} else {
		mergedOrderList = mergedOrderList.remove(mo.ID)
		return mergedOrderList, nil
	}
}

//移除集合单
func RemoveMergedOrder(mergedOrderID string) error {
	if list, err := removeMergedOrder(mergedOrderID, g_mergedOrders); err != nil {
		return err
	} else {
		g_mergedOrders = list
		return nil
	}
	// if mo := g_mergedOrders.find(mergedOrderID); mo == nil {
	// 	return ERR_NO_SUCH_MERGEDORDER
	// } else {
	// 	g_mergedOrders = g_mergedOrders.remove(mo.ID)
	// 	return nil
	// }
}

func removeOrderFromMergedOrder(orderID string, mergedOrderList MergedOrderList) error {
	mo := mergedOrderList.findContainsOrder(orderID)
	if mo == nil {
		return ERR_NO_SUCH_MERGEDORDER
	}
	return mo.removeOrder(orderID)
}

//从集合单中移除订单
func RemoveOrderFromMergedOrder(orderID string) error {
	return removeOrderFromMergedOrder(orderID, g_mergedOrders)
	// mo := g_mergedOrders.findContainsOrder(mergedOrderID)
	// if mo == nil {
	// 	return ERR_NO_SUCH_MERGEDORDER
	// }
	// return mo.removeOrder(orderID)
}

func mergeOrder(orderID, mergedOrderID string, chMergeOrder chan *MergeOrderRequestInfo) *MergeOrderInfo {
	DebugTraceF("开始合并订单  %s  到集合单 %s 中", orderID, mergedOrderID)
	request := newMergeOrderRequestInfo(orderID, mergedOrderID)
	g_chanMergeOrder <- request
	returnTicker := time.After(time.Second * 2)
	for {
		select {
		case info := <-request.chanCallback:
			return info
		case <-returnTicker:
			return newMergeOrderInfo("", ERR_TIMEOUT, nil)
		}
	}
}

//合并订单，每次加入一个订单
func MergeOrder(orderID, mergedOrderID string) *MergeOrderInfo {
	return mergeOrder(orderID, mergedOrderID, g_chanMergeOrder)
	// DebugTraceF("开始合并订单  %s  到集合单 %s 中", orderID, mergedOrderID)
	// request := newMergeOrderRequestInfo(orderID, mergedOrderID)
	// var chMergeOrder chan *MergeOrderRequestInfo = nil
	// if len(chs) <= 0 {
	// 	chMergeOrder = g_chanMergeOrder
	// } else {
	// 	chMergeOrder = chs[0]
	// }
	// chMergeOrder <- request
	// returnTicker := time.After(time.Second * 2)
	// for {
	// 	select {
	// 	case info := <-request.chanCallback:
	// 		return info
	// 	case <-returnTicker:
	// 		return newMergeOrderInfo("", ERR_TIMEOUT, nil)
	// 	}
	// }
}

// 设置集合单为完成状态
func SetMergedOrderCompleted(id string) error {
	order := g_mergedOrders.find(id)
	if order != nil {
		order.setCompleteState(true)
	} else {
		return ERR_NO_SUCH_MERGEDORDER
	}
	return nil
}
func GetAllMergedOrders() MergedOrderList {
	return g_mergedOrders
}

//获取集合单信息
func GetMergedOrderDetailByID(id string) *MergedOrder {
	return g_mergedOrders.find(id)
}

//通过集合单中的订单找到集合单
func GetMergedOrderDetailByOrderID(id string) *MergedOrder {
	return g_mergedOrders.findContainsOrder(id)
}
