package pickupTekLib

import (
	// "bufio"
	// "github.com/astaxie/beego/config"
	// "encoding/json"
	"errors"
	// "fmt"
	// "github.com/codegangsta/cli"
	// "github.com/tealeg/xlsx"
	// "github.com/ungerik/go-dry"
	// "os"
	// "strconv"
	// "strings"
	// "time"
)

var (
	ERR_NO_SUCH_ORDER            = errors.New("该订单不存在")
	ERR_NO_ORDER_ALREADY_MERGED  = errors.New("订单已经被合并过")
	ERR_NO_SUCH_MERGEDORDER      = errors.New("找不到该集合单单号")
	ERR_TIMEOUT                  = errors.New("操作超时")
	ERR_MORE_ORDERS_THAN_ALLOWED = errors.New("订单数量超出集合单最大值")
)
