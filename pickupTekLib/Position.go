package pickupTekLib

import (
	"errors"
	// "strconv"
	// "time"
	// "fmt"
)

type PositionList []*Position

func newPositionList(maxID int, shelfID string) PositionList {
	if maxID < 1 {
		return nil
	}
	list := PositionList{}
	for i := 1; i <= maxID; i++ {
		list = append(list, newPosition(i, shelfID))
	}
	return list
}

//有订单拣选的货位的数量
func (this PositionList) occupiedPosCount() (int, []string) {
	i := 0
	orderIDList := []string{}
	for _, pos := range this {
		if pos.occupied() == true {
			i += 1
			orderIDList = append(orderIDList, pos.CurrentOrder.ID)
		}
	}
	return i, orderIDList
}

//查找被该订单占据的货位
func (this PositionList) orderAlreadyLocated(orderID string) *Position {
	for _, pos := range this {
		if pos.occupied() == true && pos.CurrentOrder.ID == orderID {
			return pos
		}
	}
	return nil
}

//添加订单与货位的绑定
func (this PositionList) setOrder(order *Order) (*Position, error) {
	if order == nil {
		return nil, errors.New("参数不能为空")
	}
	for _, pos := range this {
		if pos.occupied() == false {
			if err := pos.setOrder(order); err != nil {
				return nil, err
			} else {
				return pos, nil
			}
		}
	}
	return nil, errors.New("货位没有空余")
}

//将一个商品放到一系列货位中，看是否占据货位的订单需要该商品
func (this PositionList) setProduct(productID string) (*Position, error) {
	for _, pos := range this {
		if pos.occupied() == true {
			if b, _ := pos.CurrentOrder.satisfy(productID); b == true {
				return pos, nil
			}
		}
	}
	return nil, errors.New("已分配货位的订单中均不需要本产品")
}

type Position struct {
	ID           int
	ShelfID      string
	CurrentOrder *Order //`json:"-"`
	// Null         bool
}

func (this *Position) setOrder(order *Order) error {
	if this.CurrentOrder != nil {
		return errors.New("该货位已经被占用")
	}
	this.CurrentOrder = order
	return nil
}
func (this *Position) clearCurrentOrder() {
	this.CurrentOrder = nil
}
func (this *Position) occupied() bool {
	// return this.Null == true
	return this.CurrentOrder != nil
}
func (this *Position) completed() bool {
	if this.CurrentOrder == nil {
		return true
	}
	return this.CurrentOrder.completed()
}

func (this *Position) tryComplete() bool {
	if this.CurrentOrder == nil {
		return true
	}
	if this.CurrentOrder.completed() {
		DebugInfoF("订单 %s 离开货位 %d", this.CurrentOrder.ID, this.ID)
		this.CurrentOrder = nil
		return true
	}
	return false
}

func newPosition(id int, shelfID string) *Position {
	return &Position{
		ID:      id,
		ShelfID: shelfID,
		// Null: false,
	}
}
