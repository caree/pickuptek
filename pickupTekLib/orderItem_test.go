package pickupTekLib

import (
	// "errors"
	// "fmt"
	// "github.com/tealeg/xlsx"
	// "strconv"
	// "strings"
	"testing"
	// "time"
)

var ()

func TestNewOrderItem(t *testing.T) {
	item := newOrderItem("productid1", 1, "orderid")
	if item.satisfied() == true {
		t.FailNow()
	}
	item.plusOrderedItem(1)
	if item.CountNeeded != 2 {
		t.FailNow()
	}
	item.plusCurrentCount(2)
	if item.satisfied() == false {
		t.FailNow()
	}
}
func TestOrderItemList(t *testing.T) {
	list := newOrderItemList()
	if len(list) > 0 {
		t.FailNow()
	}
	item1 := newOrderItem("productid1", 1, "order1")
	item2 := newOrderItem("productid1", 1, "order1")
	item3 := newOrderItem("productid2", 1, "order1")
	item4 := newOrderItem("productid1", 1, "order2")
	list = newOrderItemList(item1, item2, item3, item4)
	if len(list) > 3 {
		t.FailNow()
	}
}
