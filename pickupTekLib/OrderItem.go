package pickupTekLib

import (
	"errors"
	// "time"
	"fmt"
)

//订单可以重置？

type OrderItem struct {
	ProductID    string
	CountNeeded  int
	CurrentCount int
	OrderID      string //所在订单的ID，选择性使用
}

//将同类的需求数目合并，即增加需求量
func (this *OrderItem) plusOrderedItem(count int) {
	this.CountNeeded = this.CountNeeded + count
}

//需求和当前满足的数量是否一致
func (this *OrderItem) satisfied() bool {
	return this.CurrentCount == this.CountNeeded
}

//强制满足需求
func (this *OrderItem) satisfyForced() {
	this.CurrentCount = this.CountNeeded
}

//增加已经拣选的数量
func (this *OrderItem) plusCurrentCount(count int) error {
	if count <= 0 {
		return nil
	}
	if this.satisfied() == true {
		return errors.New("已经完成该项")
	}
	if this.CurrentCount+count > this.CountNeeded {
		return errors.New("超出订单项数目")
	}
	this.CurrentCount = this.CurrentCount + count
	return nil
}

//向拣选项中添加一个商品，返回是否拣选完成的状态
func (this *OrderItem) plusOneProductGetSatisfiedState() bool {
	if err := this.plusCurrentCount(1); err == nil {
		return this.satisfied()
	} else {
		return false
	}
}
func (this *OrderItem) String() string {
	return fmt.Sprintf("ProductID:  %15s OrderID: %10s 需求量: %2d  当前量: %2d", this.ProductID, this.OrderID, this.CountNeeded, this.CurrentCount)
}
func newOrderItem(productID string, countNeed int, orderID string) *OrderItem {
	return &OrderItem{
		ProductID:    productID,
		CountNeeded:  countNeed,
		CurrentCount: 0,
		OrderID:      orderID,
	}
}

type OrderItemList []*OrderItem

func (this OrderItemList) ListName() string {
	return "订单项"
}
func (this OrderItemList) InfoList() []string {
	list := []string{}
	for _, item := range this {
		list = append(list, item.String())
	}
	return list
}
func (this OrderItemList) totalItemCount() int {
	count := 0
	for _, item := range this {
		count += item.CountNeeded
	}
	return count
}
func (this OrderItemList) findByProductID(productID string) *OrderItem {
	for _, _item := range this {
		if _item.ProductID == productID {
			return _item
		}
	}
	return nil
}
func (this OrderItemList) find(productID, orderID string) (*OrderItem, int) {
	for i, _item := range this {
		if _item.ProductID == productID && _item.OrderID == orderID {
			return _item, i
		}
	}
	return nil, -1
}
func (this OrderItemList) contains(item *OrderItem) bool {
	if _, index := this.find(item.ProductID, item.OrderID); index >= 0 {
		return true
	}
	return false
}
func (this OrderItemList) remove(item *OrderItem) OrderItemList {
	if _item, index := this.find(item.ProductID, item.OrderID); _item != nil {
		return append(this[0:index], this[index+1:]...)
	}
	return this
}
func (this OrderItemList) Sum() (list OrderItemList) {
	for _, item := range this {
		if itemTemp := list.findByProductID(item.ProductID); itemTemp == nil {
			list = append(list, newOrderItem(item.ProductID, item.CountNeeded, item.OrderID))
		} else {
			itemTemp.plusOrderedItem(item.CountNeeded)
		}
	}
	return
}
func (this OrderItemList) addRangeWithSameProduct(items OrderItemList) OrderItemList {
	for _, _item := range items {
		if itemTemp := this.findByProductID(_item.ProductID); itemTemp != nil {
			itemTemp.plusOrderedItem(_item.CountNeeded)
		} else {
			// this = append(this, _item)
			this = append(this, newOrderItem(_item.ProductID, _item.CountNeeded, _item.OrderID))
		}
	}
	return this
}

func (this OrderItemList) addRange(items OrderItemList) OrderItemList {
	for _, _item := range items {
		if itemTemp, _ := this.find(_item.ProductID, _item.OrderID); itemTemp != nil {
			itemTemp.plusOrderedItem(_item.CountNeeded)
		} else {
			// this = append(this, _item)
			this = append(this, newOrderItem(_item.ProductID, _item.CountNeeded, _item.OrderID))
		}
	}
	return this
}
func (this OrderItemList) add(item *OrderItem) OrderItemList {
	if _item, _ := this.find(item.ProductID, item.OrderID); _item != nil {
		_item.plusOrderedItem(item.CountNeeded)
	} else {
		return append(this, newOrderItem(item.ProductID, item.CountNeeded, item.OrderID))
	}
	return this
}

func (this OrderItemList) fillProductName(list ProductList) OrderItemWithProductNameList {
	temp := OrderItemWithProductNameList{}
	for _, item := range this {
		var orderItemWithProductName OrderItemWithProductName
		if product := list.findByID(item.ProductID); product != nil {
			orderItemWithProductName = OrderItemWithProductName{product.Name, *item}
		} else {
			orderItemWithProductName = OrderItemWithProductName{"", *item}
		}
		temp = append(temp, &orderItemWithProductName)
	}
	return temp
}

func newOrderItemList(items ...*OrderItem) (list OrderItemList) {
	//如果发现重复项，将其合并
	for _, item := range items {
		if _item, _ := list.find(item.ProductID, item.OrderID); _item != nil {
			_item.plusOrderedItem(item.CountNeeded)
		} else {
			list = append(list, item)
		}
	}
	return
}
