package pickupTekLib

import (
	// "bufio"
	// "github.com/astaxie/beego/config"
	// "encoding/json"
	"errors"
	"fmt"
	// "github.com/codegangsta/cli"
	// "github.com/tealeg/xlsx"
	// "github.com/ungerik/go-dry"
	// "os"
	// "strconv"
	// "strings"
	// "time"
)

func GetAllOrderItemsTemp() OrderItemList {
	return g_orderItemsTemp
}
func RemoveOrder(orderID string) error {
	if o := g_orders.find(orderID); o == nil {
		return errors.New("没有找到要删除的订单")
	} else {
		g_orders = g_orders.remove(orderID)
		return nil
	}
}
func AddOrder(orderID string, productIDList []string) error {
	lenBefore := len(g_orders)
	orderItemList := OrderItemList{}
	for _, str := range productIDList {
		if len(str) > 0 {
			fmt.Println(str)
			orderItemList = orderItemList.add(newOrderItem(str, 1, orderID))
		}
	}
	if len(orderItemList) <= 0 {
		return errors.New("订单中不含任何产品")
	}
	g_orders = g_orders.add(newOrder(orderID, "测试", "00:00:00", orderItemList))
	DebugInfoF("添加订单 %s 完成, 订单数量由 %d 变为 %d ", orderID, lenBefore, len(g_orders))
	return nil
}

//将未完成的订单从总订单列表中清楚
//和已完成的订单做相同的处理，直接抛弃掉
func ClearCompletedOrders() error {
	list := OrderList{}
	for _, order := range g_orders {
		if order.completed() == false {
			list = append(list, order)
		}
	}
	g_orders = list
	return nil
}
func GetOrderInfoList(mergedOrderID string) (list OrderInfoList) {
	ordersRequired := g_orders
	if len(mergedOrderID) > 0 {
		mo := g_mergedOrders.find(mergedOrderID)
		if mo == nil {
			return
		} else {
			ordersRequired = mo.Orders
		}
	}
	// ordersRequired := OrderList{}
	// if mo != nil {
	// 	for _, order := range g_orders {
	// 		if dry.StringListContains(mo.OrderIDList, order.ID) == true {
	// 			ordersRequired = append(ordersRequired, order)
	// 		}
	// 	}
	// } else {
	// 	ordersRequired = g_orders
	// }
	for _, order := range ordersRequired {
		totalItemCount, currentItemCount := order.countItem()
		// completed := "否"
		// if totalItemCount == currentItemCount {
		// 	completed = "是"
		// }
		// located := "否"
		posID := 0
		shelfID := ""
		// if pos := Positions.OrderAlreadyLocated(order.ID); pos != nil {
		if pos := g_shelves.getOrderPos(order.ID); pos != nil {
			// located = "是"
			posID = pos.ID
			shelfID = pos.ShelfID
		}
		list = append(list, newOrderInfo(order.ID, shelfID, posID, totalItemCount, currentItemCount))
	}
	return
}

//强制完成订单，使得其包含的项目的需求和拣选数量一致
func CompleteOrderForced(orderID string) error {
	order := g_orders.find(orderID)
	if order == nil {
		return errors.New("不存在该订单")
	} else {
		order.completeForced()
		return nil
	}
}

func GetOrderByID(id string) *Order {
	return g_orders.find(id)
}
func FillOrderItemProductName(itemList OrderItemList) OrderItemWithProductNameList {
	return itemList.fillProductName(g_products)
}
