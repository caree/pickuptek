package pickupTekLib

import (
	// "bufio"
	// "github.com/astaxie/beego/config"
	// "encoding/json"
	"errors"
	"fmt"
	// "github.com/codegangsta/cli"
	"github.com/tealeg/xlsx"
	// "github.com/ungerik/go-dry"
	// "os"
	// "strconv"
	"strings"
	// "time"
)

func GetProductInfoByID(id string) *Product {
	return g_products.findByID(id)
}

func UpdateProductInfoFromFile(excelFileName string) error {
	excelFileName = "temp/" + excelFileName
	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		DebugMust(err.Error())
		return err
	}
	//检查格式
	fmt.Println(fmt.Sprintf("文件中共有 %d 个 sheet", len(xlFile.Sheets)))
	if len(xlFile.Sheets) <= 0 {
		return errors.New("文件格式错误")
	}

	if len(xlFile.Sheets) > 0 {
		firstSheet := xlFile.Sheets[0]
		if len(firstSheet.Rows) <= 0 || len(firstSheet.Cols) < 3 {
			return errors.New("文件格式错误 1")

		}
		rows := firstSheet.Rows
		if "商品编码" != strings.Trim(rows[0].Cells[2].Value, " ") ||
			"商品名称" != strings.Trim(rows[0].Cells[1].Value, " ") {
			DebugInfo(strings.Trim(rows[0].Cells[2].Value, " ") + "  " + strings.Trim(rows[0].Cells[1].Value, " "))
			return errors.New("文件格式错误 2")
		}
		DebugInfo("导入商品信息列表:")
		maxCol := 2
		addedCount := 0
		for j, row := range rows {
			if j <= 0 {
				continue
			}
			str := ""
			for i := 0; i < maxCol; i++ {
				str = str + "    " + row.Cells[i].String()
			}
			productID := strings.Trim(row.Cells[2].String(), " ")
			if len(productID) <= 0 {
				continue
			}

			if err := AddProduct(productID, strings.Trim(row.Cells[1].String(), " ")); err == nil {
				// fmt.Println(str)
				addedCount++
			} else {
				DebugInfoF(err.Error())
			}
		}
		DebugInfoF("成功添加了 %d 个产品信息", addedCount)
	}
	return nil
}

//将冻库商品名称添加到数据库中，之后将未确定的项中所有名称相同的处理完成
func ConfirmOrderItemIn(productName string) error {
	DebugInfoF("尝试添加 %s 到本库", productName)
	list := OrderItemList{}
	listLeft := OrderItemList{}
	for _, item := range g_orderItemsTemp {
		// DebugInfo(fmt.Sprintf("比较两个商品名称 %s  %s ", item.ProductID, productName) + GetFileLocation())
		if item.ProductID == productName {
			list = append(list, item)
		} else {
			listLeft = append(listLeft, item)
		}
	}
	if len(list) > 0 {
		if err := AddProductName(productName, ProductType_Frozen); err != nil {
			return err
		}
		for _, item := range list {
			//添加到订单中
			newOrderItem := newOrderItem(item.ProductID, item.CountNeeded, item.OrderID)
			if orderTemp := g_orders.find(item.OrderID); orderTemp == nil {
				g_orders = g_orders.add(newOrder(item.OrderID, "", "", OrderItemList{newOrderItem}))
			} else {
				orderTemp.addOrderItem(newOrderItem)
			}
		}
	} else {
		return errors.New("没有要处理的订单项")
	}
	g_orderItemsTemp = listLeft
	return nil
}

//将非冻库商品名称添加到数据库中，并将其余的订单项过滤掉
func ConfirmOrderItemNotIn(productName string) error {
	list := OrderItemList{}
	listLeft := OrderItemList{}
	for _, item := range g_orderItemsTemp {
		if item.ProductID == productName {
			list = append(list, item)
		} else {
			listLeft = append(listLeft, item)
		}
	}
	if len(list) > 0 {
		if err := AddProductName(productName, ProductType_Non_Frozen); err != nil {
			return err
		}
	} else {
		return errors.New("没有要处理的订单项")
	}
	g_orderItemsTemp = listLeft
	return nil
}

//如果产品名称没有在名称列表里，就应该被过滤掉，返回结果：0，在本库中；1，不在本库中；2，不确定
func getProductNameStatus(name string) int {
	for _, productName := range g_productNames {
		if productName.Name == name {
			if productName.ProductType == ProductType_Frozen {
				return Const_ProductName_IN
			} else {
				return Const_ProductName_NOT_IN
			}
		}
	}
	return Const_ProductName_NOT_SURE
}
func AddProductName(name string, productType int) error {
	if g_productNames.contains(name) == true {
		log := fmt.Sprintf("该产品名称(%s)已经添加", name)
		// DebugInfo(log + GetFileLocation())
		return errors.New(log)
	}
	// DebugTrace(fmt.Sprintf("尝试添加商品名称 %s", name) + GetFileLocation())
	productNameInfo := newProductName(name, productType)
	if err := addProductNameToDB(productNameInfo); err == nil {
		// if err := AddProductNameOnly(name, productType); err == nil {
		g_productNames = append(g_productNames, productNameInfo)
		// log := fmt.Sprintf("添加产品名称(%s)成功", name)
		// DebugInfo(log + GetFileLocation())
	} else {
		// log := fmt.Sprintf("添加产品名称(%s)失败", name)
		// DebugInfo(log + GetFileLocation())
		return err
	}
	return nil
}

type ProductNameDetail struct {
	*ProductName
	BarcodeCount int
}
type ProductNameDetailList []*ProductNameDetail

//将每个商品名称对应的条码数量计算统计出来
func GetProductNamesDetails() ProductNameDetailList {
	list := ProductNameDetailList{}
	for _, pn := range g_productNames {
		count := 0
		for _, product := range g_products {
			if product.Name == pn.Name {
				count += 1
			}
		}
		pnd := &ProductNameDetail{
			ProductName:  pn,
			BarcodeCount: count,
		}
		list = append(list, pnd)
	}
	return list
}
func AddProduct(id, name string) error {

	if g_products.contains(id, name) == true {
		DebugInfoF("该产品(%s %s)已经添加，查看ID是否重复", id, name)
		return errors.New("该产品已经添加，查看ID是否重复")
	}
	lenBefore := len(g_products)
	product := newProduct(id, name)
	if err := addProductToDB(product); err == nil {
		g_products = g_products.add(product)
		DebugInfoF("添加产品成功 产品数目由 %d 变为 %d", lenBefore, len(g_products))
	} else {
		return err
	}
	return nil
}
func RemoveProduct(id string) error {
	lenBefore := len(g_products)
	product := g_products.findByID(id)
	if product == nil {
		return Error_NoSuchProduct
	}
	if err := removeProductFromDB(product); err == nil {
		// if err := RemoveProductOnly(id); err == nil {
		g_products = g_products.remove(id)
		DebugInfoF("移除产品  %s   成功 产品数目由 %d 变为 %d", id, lenBefore, len(g_products))
	} else {
		DebugInfoF("移除产品 %s   时失败：%s", id, err.Error())
		return err
	}
	return nil
}
func RemoveProductName(name string) error {
	lenBefore := len(g_productNames)
	count := 0
	for i, _name := range g_productNames {
		if _name.Name == name {
			if err := removeProductNameFromDB(_name); err != nil {
				// if err := RemoveProductNameOnly(name); err != nil {
				return err
			} else {
				g_productNames = append(g_productNames[:i], g_productNames[i+1:]...)
				count += 1
				// return nil
			}
		}
	}
	if lenBefore != len(g_productNames) {
		DebugTraceF("产品名称数量发生变化，由 %d 变为 %d 个", lenBefore, len(g_productNames))
	}
	//相应的产品信息也需要移除
	if count > 0 {
		barcodes := g_products.getProductBarcodeListByName(name)
		for _, barcode := range barcodes {
			if err := RemoveProduct(barcode); err != nil {
				if err == Error_NoSuchProduct { //可能产品中没有该名称的产品
					return nil
				}
				return err
			}
		}
		g_products = g_products.removeByName(name)
		return nil
	} else {
		return errors.New("没有找到商品名称" + name)
	}
}
func SearchProductByKeyword(keyword string) []string {
	return g_productNames.searchKeyword(keyword)
}
func GetAllProducts() ProductList {
	return g_products
}
