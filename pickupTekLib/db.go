package pickupTekLib

import (
	"database/sql"
	// "fmt"
	"github.com/coopernurse/gorp"
	// _ "github.com/mattn/go-sqlite3"
	// "errors"
	"github.com/HouzuoGuo/tiedot/db"
	// "github.com/HouzuoGuo/tiedot/dberr"
	// "encoding/json"
	"github.com/ungerik/go-dry"
)

//==============================================================================================
var g_surport_write_db bool = true
var g_db *gorp.DbMap
var g_newDB *db.DB

func initDB() error {
	// return
	var err error
	// g_db, err = initSqliteDB()
	// if err != nil {
	// 	DebugMustF(err.Error())
	// 	return err
	// }
	g_newDB, err = initDb()
	if err != nil {
		DebugMustF(err.Error())
		return err
	}
	return nil
}

//==============================================================================================
//添加产品信息到数据库
//注意不检查是否重复!!!
func addProductToDB(product *Product) error {
	products := g_newDB.Use("products")
	if docID, err := products.Insert(product.toMapInterface()); err != nil {
		DebugMustF("向数据库添加产品信息失败")
		return err
	} else {
		product.docID = docID
		// readBack, err := products.Read(docID)
		// if err != nil {
		// 	DebugMustF("读取产品信息失败")
		// 	return err
		// }
		// fmt.Println("Document", docID, "is", readBack)
	}
	return nil
}
func getAllProductsFromDB() (ProductList, error) {
	var productList ProductList

	queryResult := make(map[int]struct{}) // query result (document IDs) goes into map keys
	products := g_newDB.Use("products")

	if err := db.EvalQuery("all", products, &queryResult); err != nil {
		return nil, err
	}
	// fmt.Println(queryResult)
	for key, _ := range queryResult {
		readBack, err := products.Read(key)
		if err != nil {
			DebugMustF("读取产品信息失败")
			continue
		}
		// fmt.Println("Document", key, "is", readBack)
		// fmt.Println("------------------------------")
		barcode, ok := readBack["Barcode"].(string)
		if ok == false {
			DebugSysF("转化产品信息的编码时出错：%v", readBack["Barcode"])
			continue
		}
		name, ok := readBack["Name"].(string)
		if ok == false {
			DebugSysF("转化产品信息的名称时出错：%v", readBack["Name"])
			continue
		}
		productList = append(productList, newProductWithDocID(barcode, name, key))
	}
	return productList, nil

	// var products ProductList
	// if _, err := g_db.Select(&products, "select * from products"); err != nil {
	// 	return nil, err
	// } else {
	// 	return products, nil
	// }
}
func removeProductFromDB(product *Product) error {
	productCol := g_newDB.Use("products")
	if err := productCol.Delete(product.docID); err != nil {
		DebugSysF("从数据库删除产品信息时出错：%s", err.Error())
		return err
	}
	return nil
}

// func RemoveProductOnly(productID string) error {
// 	if g_surport_write_db == false {
// 		return nil
// 	}

// 	if _, err := g_db.Exec(fmt.Sprintf("delete from products where Barcode = '%s'", productID)); err != nil {
// 		return err
// 	} else {
// 		return nil
// 	}
// }
// func AddProductOnly(product *Product) error {
// 	if g_surport_write_db == false {
// 		return nil
// 	}

// 	err := g_db.Insert(product)
// 	if err != nil {
// 		return err
// 	} else {
// 		return nil
// 	}
// }
func removeProductNameFromDB(productNameInfo *ProductName) error {
	productNameCol := g_newDB.Use("productName")
	if err := productNameCol.Delete(productNameInfo.docID); err != nil {
		DebugSysF("从数据库删除产品名称信息时出错：%s", err.Error())
		return err
	}
	return nil
}

// func RemoveProductNameOnly(name string) error {
// 	if g_surport_write_db == false {
// 		return nil
// 	}

// 	if _, err := g_db.Exec(fmt.Sprintf("delete from productName where Name = '%s'", name)); err != nil {
// 		return err
// 	} else {
// 		return nil
// 	}
// }

// 添加产品名称信息到数据库
// 不检查重复！！！！
func addProductNameToDB(productNameInfo *ProductName) error {
	productName := g_newDB.Use("productName")
	if docID, err := productName.Insert(productNameInfo.toMapInterface()); err != nil {
		DebugMustF("向数据库添加产品信息失败")
		return err
	} else {
		productNameInfo.docID = docID
		// readBack, err := productName.Read(docID)
		// if err != nil {
		// 	DebugMustF("读取产品名称信息失败")
		// 	return err
		// }
		// fmt.Println("Document", docID, "is", readBack)
	}

	return nil
}

// func AddProductNameOnly(name string, productType int) error {
// 	if g_surport_write_db == false {
// 		return nil
// 	}

// 	err := g_db.Insert(NewProductName(name, productType))
// 	if err != nil {
// 		return err
// 	} else {
// 		return nil
// 	}
// }
func getAllProductNameFromDB() (ProductNameList, error) {
	var list ProductNameList

	queryResult := make(map[int]struct{}) // query result (document IDs) goes into map keys
	productNameCol := g_newDB.Use("productName")
	if err := db.EvalQuery("all", productNameCol, &queryResult); err != nil {
		return nil, err
	}
	// fmt.Println(queryResult)
	for key, _ := range queryResult {
		readBack, err := productNameCol.Read(key)
		if err != nil {
			DebugMustF("读取产品名称失败")
			continue
		}
		// DebugTraceF("%#v", readBack)
		name, ok := readBack["Name"].(string)
		if ok == false {
			DebugSysF("转化产品名称的名字时出错：%v", readBack["Name"])
			continue
		}
		// switch t := (readBack["ProductType"]).(type) {
		// default:
		// 	DebugTraceF("=> %T", t)
		// }
		productType, ok := (readBack["ProductType"]).(float64)
		if ok == false {
			DebugSysF("转化产品名称的类型时出错：%T -> %+v", productType, readBack["ProductType"])
			continue
		}

		list = append(list, newProductNameWithDocID(name, int(productType), key))
	}
	return list, nil

	// var list ProductNameList
	// if _, err := g_db.Select(&list, "select Name from productName"); err != nil {
	// 	return nil, err
	// } else {
	// 	return list, nil
	// }
}

//==============================================================================================

func initDb() (*db.DB, error) {
	// func initDb() (*gorp.DbMap, error) {

	myDBDir := "./db"

	// (Create if not exist) open a database
	dbTemp, err := db.OpenDB(myDBDir)
	if err != nil {
		return nil, err
	}
	collectionTable := []string{"products", "productName"}
	collections := dbTemp.AllCols()

	for _, col := range collectionTable {
		if dry.StringInSlice(col, collections) == false {
			if err := dbTemp.Create(col); err != nil {
				return nil, err
			}
		}
	}
	return dbTemp, nil

}
func initSqliteDB() (*gorp.DbMap, error) {
	// connect to db using standard Go database/sql API
	// use whatever database/sql driver you wish
	db, err := sql.Open("sqlite3", "./db.db3")
	if err != nil {
		return nil, err
	}

	// construct a gorp DbMap
	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.SqliteDialect{}}

	// add a table, setting the table name to 'posts' and
	// specifying that the Id property is an auto incrementing PK
	dbmap.AddTableWithName(Product{}, "products").SetKeys(false, "Barcode", "Name")
	dbmap.AddTableWithName(ProductName{}, "productName").SetKeys(false, "Name")
	// return nil
	// create the table. in a production system you'd generally
	// use a migration tool, or create the tables via scripts
	if err := dbmap.CreateTablesIfNotExists(); err != nil {
		// DebugOutput("创建数据库表失败"+getFileLocation(), 1)
		return nil, err
		// checkErr(err, "Create tables failed")
	}

	return dbmap, nil
}
