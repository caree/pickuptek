package pickupTekLib

import (
	// "errors"
	// "fmt"
	// "github.com/tealeg/xlsx"
	// "strconv"
	// "strings"
	// "github.com/ungerik/go-dry"
	"testing"
)

var ()

func init() {
	SetdebugLevel(0)
}

func Test改变集合单全局变量(t *testing.T) {
	p_mergedOrders := &MergedOrderList{}
	// fmt.Printf("%T  %s", p_mergedOrders, p_mergedOrders)
	if len(*p_mergedOrders) > 0 {
		panic("数量应该为0")
	}
	list := changeMolist(*p_mergedOrders)
	p_mergedOrders = &list
	if len(*p_mergedOrders) <= 0 {
		panic("数量应该为1")
	}
	// panic("")
}
func changeMolist(list MergedOrderList) MergedOrderList {
	mo, _ := newMergedOrder("001", OrderList{}, 3)
	l := append(list, mo)
	return l
}
func Test合成集合单(t *testing.T) {
	var err error
	orders := OrderList{}
	order1 := newOrder("o1", "", "", newOrderItemList(newOrderItem("p001", 1, "")))
	moRequest := newMergeOrderRequestInfo(order1.ID, "") //
	mergedOrders := MergedOrderList{}

	err = removeOrderFromMergedOrder(order1.ID, mergedOrders)
	if err == nil {
		t.Fatalf("应该提示错误 :%s", ERR_NO_SUCH_ORDER)
	}

	//测试合并错误订单
	moi, _ := doMergeOrderEvents(moRequest, orders, 2, mergedOrders)

	if moi.Err != ERR_NO_SUCH_ORDER {
		t.Fatalf("应该提示错误 :%s", ERR_NO_SUCH_ORDER)
	}
	//可以正确合并
	orders = orders.add(order1)
	moi, mergedOrders = doMergeOrderEvents(moRequest, orders, 2, mergedOrders)
	if moi.Err != nil {
		t.Fatalf("不应该出错: %s", moi.Err)
	}
	if b, _ := mergedOrders.orderExists(order1.ID); b == false {
		t.Fatalf("订单 %s 应该已经被合并", order1.ID)
	}

	moiID := moi.MergedOrder.ID
	moRequest = newMergeOrderRequestInfo(order1.ID, moiID) //
	//测试订单重复合并
	moi, mergedOrders = doMergeOrderEvents(moRequest, orders, 2, mergedOrders)
	if moi.Err != ERR_NO_ORDER_ALREADY_MERGED {
		t.Fatalf("应该提示错误 :%s", ERR_NO_ORDER_ALREADY_MERGED)
	}

	err = removeOrderFromMergedOrder(order1.ID, mergedOrders)
	if err != nil {
		t.Log(moiID)
		t.Log(mergedOrders)
		t.Fatalf("不应该出错: %s", err)
	}
	if b, _ := mergedOrders.orderExists(order1.ID); b == true {
		t.Fatalf("订单 %s 应该已经被移除", order1.ID)
	}

	moi, mergedOrders = doMergeOrderEvents(moRequest, orders, 2, mergedOrders)
	if moi.Err != nil {
		t.Fatalf("不应该出错: %s", moi.Err)
	}
	if len(mergedOrders) > 1 {
		t.Fatalf("集合单数量应为 1，而实际为 %d", len(mergedOrders))
	}

	order2 := newOrder("o2", "", "", newOrderItemList(newOrderItem("p001", 1, "")))
	orders = orders.add(order2)
	//测试合并到错误或者不存在的集合单
	moRequest = newMergeOrderRequestInfo(order2.ID, "mo001") //
	moi, mergedOrders = doMergeOrderEvents(moRequest, orders, 2, mergedOrders)
	if moi.Err != ERR_NO_SUCH_MERGEDORDER {
		t.Fatalf("应该提示错误 :%s", ERR_NO_SUCH_MERGEDORDER)
	}
	if len(mergedOrders) > 1 {
		t.Fatalf("集合单数量应为 1，而实际为 %d", len(mergedOrders))
	}
	//将订单合并到已有集合单
	moRequest = newMergeOrderRequestInfo(order2.ID, moiID) //
	moi, mergedOrders = doMergeOrderEvents(moRequest, orders, 2, mergedOrders)
	if moi.Err != nil {
		t.Fatalf("不应该出错: %s", moi.Err)
	}
	t.Log(mergedOrders)

	mo := mergedOrders.findContainsOrder(order1.ID)
	if mo == nil {
		t.Fatalf("应该存在集合单")
	}
	o := mo.Orders.find("o1")
	if o.Items[0].CountNeeded > 1 {
		t.Log(o.Items)
		t.Fatal("订单需求量错误")
	}

	//超过集合单最大订单数
	order3 := newOrder("o3", "", "", newOrderItemList(newOrderItem("p001", 1, "")))
	orders = orders.add(order3)
	moRequest = newMergeOrderRequestInfo(order3.ID, moiID) //
	moi, _ = doMergeOrderEvents(moRequest, orders, 2, mergedOrders)
	if moi.Err != ERR_MORE_ORDERS_THAN_ALLOWED {
		t.Log(moi.Err)
		t.Log(mergedOrders)
		t.Fatalf("应该提示错误 :%s ", ERR_MORE_ORDERS_THAN_ALLOWED)
	}
}

// func test移除集合单中的订单(t *testing.T) {
// 	orders := OrderList{}
// 	order1 := newOrder("o1", "", "", newOrderItemList(newOrderItem("p001", 1, "")))
// 	mo, err := newMergedOrder(order1.ID, orders, 3)
// 	if err != nil {

// 	}
// }
func test订单切分成集合单(t *testing.T) {
	orders := OrderList{}
	orders = orders.add(newOrder("o1", "", "", newOrderItemList(newOrderItem("p001", 1, ""))))
	orders = orders.add(newOrder("o2", "", "", newOrderItemList(newOrderItem("p002", 2, ""))))
	orders = orders.add(newOrder("o3", "", "", newOrderItemList(newOrderItem("p003", 1, ""))))
	orders = orders.add(newOrder("o4", "", "", newOrderItemList(newOrderItem("p003", 1, ""))))
	orders = orders.add(newOrder("o5", "", "", newOrderItemList(newOrderItem("p003", 1, ""))))

	listlist := orders.split(3)
	if len(listlist) != 2 {
		t.Fatalf("应该分为两组")
	}
	if len(listlist[0]) != 3 {
		t.Fatalf("订单数量不对，应该为3")
	}
	if len(listlist[1]) != 2 {
		t.Fatalf("订单数量不对，应该为2")
	}
}
