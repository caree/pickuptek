package pickupTekLib

import (
// "bufio"
// "github.com/astaxie/beego/config"
// "encoding/json"
// "errors"
// "fmt"
// "github.com/codegangsta/cli"
// "github.com/tealeg/xlsx"
// "os"
// "strconv"
// "strings"
// "time"
)

type MergeOrderRequestInfo struct {
	OrderID       string
	MergedOrderID string
	chanCallback  chan *MergeOrderInfo
}

func newMergeOrderRequestInfo(orderID, mergedOrderID string) *MergeOrderRequestInfo {
	return &MergeOrderRequestInfo{
		OrderID:       orderID,
		MergedOrderID: mergedOrderID,
		chanCallback:  make(chan *MergeOrderInfo),
	}
}
