package pickupTekLib

import (
// "bufio"
// "github.com/astaxie/beego/config"
// "encoding/json"
// "errors"
// "fmt"
// "github.com/codegangsta/cli"
// "github.com/tealeg/xlsx"
// "os"
// "strconv"
// "strings"
// "time"
)

//对外输出订单信息的结构
type OrderInfo struct {
	ID               string
	TotalItemCount   int //包含的商品的总数量
	CurrentItemCount int //已经拣选的商品的数量
	PositionID       int
	ShelfID          string
	// Completed        string //是 否
	// Located          string //是 否
}

func newOrderInfo(orderID, shelfID string, posID, totalItemCount, currentItemCount int) *OrderInfo {
	return &OrderInfo{
		ID:               orderID,
		ShelfID:          shelfID,
		TotalItemCount:   totalItemCount,
		CurrentItemCount: currentItemCount,
		PositionID:       posID,
		// Completed:        completed,
		// Located:          located,
	}
}

type OrderInfoList []*OrderInfo
