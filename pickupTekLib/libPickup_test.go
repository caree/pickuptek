package pickupTekLib

import (
	// "errors"
	// "fmt"
	// "github.com/tealeg/xlsx"
	// "strconv"
	// "strings"
	"testing"
)

/*
系统运作流程
准备工作：
	* 批量导入订单，系统对订单进行预处理
流程：
	* 扫描订单或者产品条码，将编号发给系统
	* 系统接收编号后，首先确定是否为订单编号，方法是查找是否存在此编号的订单；
		* 是订单，查找是否已经为该订单分配货位，没有，则分配货位
		* 不是订单，查找是否已经分配货位的订单中需要此产品
			* 需要，修改该订单对该产品的需求量，并检查该订单是否拣选完成
				* 完成，提示
				* 未完成
			* 不需要，提示错误


*/

var ()

func init() {

}

func Test测试分配货位(t *testing.T) {
	orders := OrderList{}
	orders = orders.add(newOrder("o1", "", "", newOrderItemList(newOrderItem("p001", 1, ""))))
	orders = orders.add(newOrder("o2", "", "", newOrderItemList(newOrderItem("p002", 2, ""))))
	orders = orders.add(newOrder("o3", "", "", newOrderItemList(newOrderItem("p003", 1, ""))))

	positions := newPositionList(5, "1")
	orderID := "o2"
	if orders.containsID(orderID) == false {
		t.Log("无法查询到正确订单")
		t.FailNow()
	} else {
		if _, err := positions.setOrder(orders.find(orderID)); err != nil {
			t.Log("未能分配货位")
			t.FailNow()
		}
	}
}
func Test订单完成只有一个(t *testing.T) {
	orders := OrderList{}
	orders = orders.add(newOrder("o1", "", "", newOrderItemList(newOrderItem("p001", 1, ""))))
	positions := newPositionList(5, "1")
	positions.setOrder(orders.find("o1"))
	productID := "p001"
	if pos, err := positions.setProduct(productID); err != nil {
		t.Log("拣选失败")
		t.FailNow()
	} else {
		if pos.completed() == false {
			t.Log("订单应该已经完成")
			t.FailNow()
		}
	}
}
func Test订单完成(t *testing.T) {
	orders := OrderList{}
	orders = orders.add(newOrder("o1", "", "", newOrderItemList(newOrderItem("p001", 1, ""))))
	orders = orders.add(newOrder("o2", "", "", newOrderItemList(newOrderItem("p002", 2, ""))))
	orders = orders.add(newOrder("o3", "", "", newOrderItemList(newOrderItem("p003", 1, ""))))

	positions := newPositionList(5, "1")
	positions.setOrder(orders.find("o1"))
	positions.setOrder(orders.find("o2"))
	positions.setOrder(orders.find("o3"))
	productID := "p002"
	if pos, err := positions.setProduct(productID); err != nil {
		t.Log("拣选失败")
		t.FailNow()
	} else {
		if pos.completed() == true {
			t.Log("订单应该未完成")
			t.FailNow()
		}
	}
	productID = "p001"
	if pos, err := positions.setProduct(productID); err != nil {
		t.Log("拣选失败")
		t.FailNow()
	} else {
		if pos.completed() == false {
			t.Log("订单应该已经完成")
			t.FailNow()
		}
	}
	productID = "p002"
	if pos, err := positions.setProduct(productID); err != nil {
		t.Log("拣选失败")
		t.FailNow()
	} else {
		if pos.completed() == false {
			t.Log("订单应该已经完成")
			t.FailNow()
		}
	}
}
func Test订单多于货位(t *testing.T) {
	orders := OrderList{}
	orders = orders.add(newOrder("o1", "", "", newOrderItemList(newOrderItem("p001", 1, ""))))
	orders = orders.add(newOrder("o2", "", "", newOrderItemList(newOrderItem("p002", 2, ""))))
	orders = orders.add(newOrder("o3", "", "", newOrderItemList(newOrderItem("p003", 1, ""))))

	positions := newPositionList(1, "")
	positions.setOrder(orders.find("o1"))
	if _, err := positions.setOrder(orders.find("o2")); err == nil {
		t.Log("应该没有货位分配")
		t.FailNow()
	}

	productID := "p001"
	if pos, err := positions.setProduct(productID); err != nil {
		t.Log("拣选失败")
		t.FailNow()
	} else {
		if pos.completed() == false {
			t.Log("订单应该已经完成")
			t.FailNow()
		} else {
			pos.tryComplete()
		}
	}
	if _, err := positions.setOrder(orders.find("o2")); err != nil {
		t.Log("应该有货位分配")
		t.FailNow()
	}
}
