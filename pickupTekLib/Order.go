package pickupTekLib

import (
	// "errors"
	"fmt"
	"strconv"
	"time"
)

// type OrderListPointer *OrderList

// func (this *OrderListPointer) addOrder(order *Order) {
// 	// list := *this
// 	// list = append(list, order)
// 	// this = &list
// }

//订单可以重置？

type Order struct {
	ID           string
	Deliver      string
	DelivingTime string
	Items        OrderItemList
}

func newOrder(id, diliver, delivingTime string, items OrderItemList) *Order {
	if items == nil {
		items = OrderItemList{}
	}
	return &Order{
		ID:           id,
		Deliver:      diliver,
		DelivingTime: delivingTime,
		Items:        items,
	}
}
func (this *Order) String() string {
	// DebugTraceF("订单信息  ID: %s  Deliver: %s  DelivingTime: %s", this.ID, this.Deliver, this.DelivingTime)
	// this.Items.Print()
	str := fmt.Sprintf("订单信息  ID: %s  Deliver: %s  DelivingTime: %s  订单项数量：%d",
		this.ID, this.Deliver, this.DelivingTime, len(this.Items))
	// str = fmt.Sprintf("%s \r\n    %s", str, this.Items.ListName())
	// list := this.Items.InfoList()
	// for _, s := range list {
	// 	str = fmt.Sprintf("%s \r\n    %s", str, s)
	// }
	return str
}
func (this *Order) countItem() (totalCount, currentCount int) {
	for _, _item := range this.Items {
		totalCount += _item.CountNeeded
		currentCount += _item.CurrentCount
	}
	return totalCount, currentCount
}
func (this *Order) completed() bool {
	for _, _item := range this.Items {
		if _item.satisfied() == false {
			return false
		}
	}
	return true
}
func (this *Order) completeForced() {
	for _, _item := range this.Items {
		_item.satisfyForced()
	}
}
func (this *Order) satisfy(productID string) (bool, error) {
	for _, _item := range this.Items {
		if _item.ProductID == productID {
			if _item.satisfied() == false {
				if err := _item.plusCurrentCount(1); err != nil {
					return false, err
				} else {
					return true, nil
				}
			}
			break
		}
	}
	return false, nil
}
func (this *Order) addOrderItem(item *OrderItem) {
	this.Items = this.Items.add(item)
}
func (this *Order) addOrderItems(items OrderItemList) {
	for _, item := range items {
		this.addOrderItem(item)
	}
}
func (this *Order) removeOrderItem(item *OrderItem) {
	this.Items = this.Items.remove(item)
}

type OrderList []*Order

func (this OrderList) ListName() string {
	return "订单"
}
func (this OrderList) InfoList() []string {
	list := []string{}
	for _, order := range this {
		list = append(list, order.String())
	}
	return list
}
func (this OrderList) allOrderItems() (items OrderItemList) {
	for _, order := range this {
		items = append(items, order.Items...)
	}
	return
}

//之前存在的订单不会重复添加
func (this OrderList) addRange(orders OrderList) OrderList {
	tempList := this
	for _, order := range orders {
		if tempList.contains(order) == false {
			tempList = append(tempList, order)
		}
	}
	return tempList
}
func (this OrderList) add(orders ...*Order) OrderList {
	tempList := this
	for _, order := range orders {
		if tempList.contains(order) == false {
			tempList = append(tempList, order)
		}
	}
	return tempList
}
func (this OrderList) contains(order *Order) bool {
	return this.containsID(order.ID)
}
func (this OrderList) find(id string) *Order {
	for _, _order := range this {
		if _order.ID == id {
			return _order
		}
	}
	return nil
}
func (this OrderList) containsID(id string) bool {
	if orderTemp := this.find(id); orderTemp == nil {
		return false
	} else {
		return true
	}
}

func (this OrderList) remove(id string) OrderList {
	for i, _order := range this {
		if _order.ID == id {
			return append(this[0:i], this[i+1:]...)
		}
	}
	return this
}
func (this OrderList) satisfy(productID string) (bool, *Order) {
	for _, _order := range this {
		if b, _ := _order.satisfy(productID); b == true {
			return true, _order
		}
	}
	return false, nil
}
func (this OrderList) merge(maxCount int) (*MergedOrder, error) {
	if len(this) > maxCount {
		return nil, ERR_MORE_ORDERS_THAN_ALLOWED
	}
	orderItems := OrderItemList{}
	idList := []string{}
	for _, _order := range this {
		// orderItems = _order.Items.addRange(orderItems)
		orderItems = orderItems.addRangeWithSameProduct(_order.Items)
		idList = append(idList, _order.ID)
	}
	return newMergedOrder(strconv.Itoa(time.Now().Nanosecond()), this, maxCount)
}
func (this OrderList) split(count int) []OrderList {
	list := []OrderList{}
	temp := this
	for {
		// fmt.Println("长度为 ", len(temp))
		if len(temp) <= count {
			list = append(list, temp)
			break
		} else {
			list = append(list, temp[:count])
			temp = temp[count:]
		}
	}
	return list
}
