package controllers

import (
	// "database/sql"
	"fmt"
	// "github.com/coopernurse/gorp"
	// _ "github.com/mattn/go-sqlite3"
	// "errors"
	// "github.com/HouzuoGuo/tiedot/db"
	// "github.com/HouzuoGuo/tiedot/dberr"
	// "encoding/json"
	// "github.com/ungerik/go-dry"
)

//系统配置项
type Config struct {
	UpdaterPort    string
	ShelfCount     int      //拣选墙数目
	LocationCount  int      //每个拣选墙的货位数
	OrderPrefix    string   //订单的前缀
	FilterKeywords []string //忽略指定关键字的产品
}

func (this *Config) ListName() string {
	return "系统配置"
}
func (this *Config) InfoList() []string {
	list := []string{
		fmt.Sprintf("拣选墙数量：%d", this.ShelfCount),
		fmt.Sprintf("每个拣选墙货位数量：%d", this.LocationCount),
		fmt.Sprintf("应用升级端口：%s", this.UpdaterPort),
		fmt.Sprintf("订单前缀：%s", this.OrderPrefix),
		fmt.Sprintf("需要过滤的产品的关键字：%v ", this.FilterKeywords),
	}
	return list
}
