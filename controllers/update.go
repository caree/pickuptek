package controllers

import (
	// "fmt"
	"github.com/astaxie/beego"
	// "github.com/ungerik/go-dry"
	// "path"
	// "github.com/BurntSushi/toml"
	// "io/ioutil"
	"net/http"
	"os"
	// "path/filepath"
	// "pickupTek/pickupTekLib"
	// "strconv"
	// "strings"
)

//本系统退出，保证升级完成
func (this *MainController) ExitSelf() {
	os.Exit(0)
}
func (this *MainController) TestAlive() {
	this.Data["json"] = newCommand(0, "")
	this.ServeJson()
}

//升级助手通知本系统，升级准备完毕
func (this *MainController) Update() {
	G_updatePrepared = true
	this.Data["json"] = newCommand(0, "")
	this.ServeJson()
}

//与人交互，使用者操作本系统，通知升级助手可以对本系统进行升级。
func (this *MainController) UpdateNow() {
	resp, err := http.Get("http://localhost:" + G_updaterport + "/StartUpdate?App=" + beego.AppName)
	if err != nil {
		DebugSysF("App无法接收升级提示: %s", err.Error())
	}
	if resp.StatusCode != 200 {
		DebugSysF("App无法接收升级提示: %s", resp.Status)
	}
	this.Data["json"] = newCommand(0, "")
	this.ServeJson()
}

//查看是否有升级信息
func (this *MainController) NewUpdate() {
	this.Ctx.Output.Header("Content-Type", "text/event-stream")
	this.Ctx.Output.Header("Cache-Control", "no-cache")
	// return
	if G_updatePrepared == true {
		this.Ctx.Output.Body([]byte("event:update\ndata:ok\r\n\r\n"))
		DebugTraceF("输出升级事件")
		// this.Data["json"] = newCommand(0, "")
	} else {
		// this.Data["json"] = newCommand(1, "")
		this.Ctx.Output.Body([]byte("event:noupdate\ndata:ok\r\n\r\n"))
	}
	// this.ServeJson()
}
