package controllers

import (
	"fmt"
	// "github.com/astaxie/beego"
	"github.com/ungerik/go-dry"
	// "path"
	// "github.com/BurntSushi/toml"
	// "io/ioutil"
	// "net/http"
	"os"
	"path/filepath"
	"pickupTek/pickupTekLib"
	// "strconv"
	"strings"
)

func (this *MainController) AddOrderIndex() {
	this.TplNames = "AddOrderIndex.tpl"
}

func (this *MainController) OrderListIndex() {
	this.TplNames = "OrderListIndex.tpl"
}
func (this *MainController) NotConfirmedOrderItemsIndex() {
	this.TplNames = "NotConfirmedOrderItemsIndex.tpl"
}
func (this *MainController) OrderDetailIndex() {
	orderID := this.GetString("ID")
	if len(orderID) <= 0 {
		DebugSysF("无法获取参数 订单编号")
	}
	this.Data["orderID"] = orderID
	this.TplNames = "OrderDetailIndex.tpl"
}
func (this *MainController) CompleteOrderForced() {
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	ID := this.GetString("ID")
	if len(ID) <= 0 {
		DebugSysF("无法获取参数 订单编号")
	}
	if err := pickupTekLib.CompleteOrderForced(ID); err != nil {
		cmd = newCommand(1, err.Error())
		return
	}
}

// 订单详情
func (this *MainController) OrderItemsDetail() {
	orderID := this.GetString("ID")
	items := pickupTekLib.OrderItemWithProductNameList{}
	defer func() {
		this.Data["json"] = items
		this.ServeJson()
	}()
	if len(orderID) <= 0 {
		return
	}
	order := pickupTekLib.GetOrderByID(orderID)
	if order != nil {
		items = pickupTekLib.FillOrderItemProductName(order.Items)
	}
	// if orderFinded := pickupTekLib.Orders.Find(orderID); orderFinded != nil {
	// 	// items = pickupTekLib.FillOrderItemProductName(orderFinded.Items)
	// 	this.Data["json"] = orderFinded.Items
	// }
}

func (this *MainController) ConfirmOrderItemNotIn() {
	// OrderID := this.GetString("OrderID")
	ProductID := this.GetString("ProductID")
	DebugInfoF("试图将产品 %s 从本库中隔离", ProductID)
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	if err := pickupTekLib.ConfirmOrderItemNotIn(ProductID); err != nil {
		cmd = newCommand(1, err.Error())
		return
	}
}
func (this *MainController) ConfirmOrderItemIn() {
	// OrderID := this.GetString("OrderID")
	ProductID := this.GetString("ProductID")
	// CountNeeded := this.GetString("CountNeeded")
	DebugInfoF("试图将产品 %s 加入到本库中", ProductID)
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	if err := pickupTekLib.ConfirmOrderItemIn(ProductID); err != nil {
		cmd = newCommand(1, err.Error())
		return
	}
}

// 订单详情
func (this *MainController) OrderDetail() {
	orderID := this.GetString("ID")
	orderTemp := pickupTekLib.GetOrderByID(orderID)
	defer func() {
		this.Data["json"] = orderTemp
		this.ServeJson()
	}()
	// if len(orderID) <= 0 {
	// 	return
	// }
	// if orderFinded := pickupTekLib.Orders.Find(orderID); orderFinded != nil {
	// 	orderTemp = orderFinded
	// }
}
func (this *MainController) OrderInfoList() {
	mergedOrderID := this.GetString("mergedOrderID")
	this.Data["json"] = pickupTekLib.GetOrderInfoList(mergedOrderID)
	this.ServeJson()
}
func (this *MainController) ClearCompletedOrders() {
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	if err := pickupTekLib.ClearCompletedOrders(); err != nil {
		cmd = newCommand(1, err.Error())
		return
	}
}
func (this *MainController) RemoveOrder() {
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	orderID := this.GetString("ID")
	if err := pickupTekLib.RemoveOrder(orderID); err != nil {
		cmd = newCommand(1, err.Error())
		return
	}
}
func (this *MainController) AddOrder() {

	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	orderID := this.GetString("order")
	listData := this.GetString("list")
	fmt.Println(fmt.Sprintf("orderID: %s  list: %s", orderID, listData))
	listData = strings.Replace(listData, "，", ",", -1)
	list := strings.Split(listData, ",")
	fmt.Println(list)
	if len(list) <= 0 {
		cmd = newCommand(1, "没有订购任何项目")
		return
	}
	if err := pickupTekLib.AddOrder(orderID, list); err != nil {
		cmd = newCommand(1, err.Error())
		return
	}
	// fmt.Println("data =>")
	// fmt.Println(data)
}
func (this *MainController) UploadOrderInfo() {
	fileName := "" //上传的文件名，放置到暂存区
	cmd := newCommand(0, "")
	defer func() {
		if len(fileName) > 0 {
			//删除暂存文件
			if dry.FileExists("temp/"+fileName) == true {
				if err := os.Remove("temp/" + fileName); err != nil {
					pickupTekLib.DebugMust("删除暂存文件时出错：" + err.Error())
				} else {
					// pickupTekLib.DebugSys("删除文件 " + fileName + " 成功" + pickupTekLib.GetFileLocation())
				}
			}
		}
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	fileType := strings.Trim(this.GetString("type"), " ")
	if len(fileType) <= 0 {
		cmd = newCommand(1, "需要确定导入订单所使用的文件类型")
		return
	}
	fromFile := "fileName" + fileType //html页面设置
	_, header, err := this.GetFile(fromFile)
	if err != nil {
		DebugSysF(err.Error())
		cmd = newCommand(1, "数据导入失败")
		return
	}

	if header != nil {
		// pickupTekLib.DebugInfo(header.Filename)
		// ext := path.Ext(header.Filename)
		fileName = header.Filename
		DebugTraceF("上传文件：%s    扩展名：%s", fileName, filepath.Ext(fileName))

		if filepath.Ext(fileName) != ".xlsx" {
			cmd = newCommand(1, "文件类型错误，应为xlsx类型")
			return
		}
		if err := this.SaveToFile(fromFile, "temp/"+fileName); err != nil {
			DebugSysF("保存文件出错：" + err.Error())
			cmd = newCommand(1, "上传文件出错")
			return
		} else {
			if err := pickupTekLib.UploadOrderInfoFromFile(fileName, fileType, G_conf.OrderPrefix); err != nil {
				if err == pickupTekLib.Error_ThereIsFiltedOrders {
					cmd = newCommand(2, "")
					return
				} else {
					DebugSysF(err.Error())
					cmd = newCommand(1, err.Error())
					return
				}
			}
		}
	} else {
		pickupTekLib.DebugSys("没有文件上传")
		cmd = newCommand(1, "上传文件出错")
		return
	}
}
