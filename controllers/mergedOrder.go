package controllers

import (
	// "fmt"
	// "github.com/astaxie/beego"
	// "github.com/ungerik/go-dry"
	// "path"
	// "github.com/BurntSushi/toml"
	// "io/ioutil"
	// "net/http"
	// "os"
	// "path/filepath"
	"pickupTek/pickupTekLib"
	// "strconv"
	// "strings"
)

func (this *MainController) PickUpMergedOrderIndex() {
	this.Data["OrderID"] = this.GetString("ID")
	this.TplNames = "PickUpMergedOrderIndex.tpl"
}

func (this *MainController) MergeOrderManagementIndex() {
	this.TplNames = "MergeOrderManagementIndex.tpl"
}
func (this *MainController) MergedOrdersIndex() {
	this.TplNames = "MergedOrdersIndex.tpl"
}
func (this *MainController) MergedOrderCompletedIndex() {
	this.Data["OrderID"] = this.GetString("ID")
	this.TplNames = "MergedOrderCompletedIndex.tpl"
}
func (this *MainController) MergeOrderIndex() {
	this.Data["mergedOrderID"] = this.GetString("mergedOrderID")

	this.TplNames = "MergeOrderIndex.tpl"
}
func (this *MainController) EditMergeOrderIndex() {
	this.Data["mergedOrderID"] = this.GetString("mergedOrderID")

	this.TplNames = "EditMergeOrderIndex.tpl"
}
func (this *MainController) RemoveOrderFromMergedOrder() {
	orderID := this.GetString("orderID")
	// mergedOrderID := this.GetString("mergedOrderID")
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	if err := pickupTekLib.RemoveOrderFromMergedOrder(orderID); err != nil {
		cmd = newCommand(1, err.Error())
		return
	}
}

//删除集合单
func (this *MainController) RemoveMergedOrder() {
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	ID := this.GetString("ID")
	if err := pickupTekLib.RemoveMergedOrder(ID); err != nil {
		cmd = newCommand(1, err.Error())
		return
	}
}

//进行订单的合并
func (this *MainController) MergeOrder() {
	cmd := newCommand(1, "系统异常")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()

	orderID := this.GetString("orderID")
	mergedOrderID := this.GetString("mergedOrderID")
	mi := pickupTekLib.MergeOrder(orderID, mergedOrderID)

	if mi.Err != nil {
		cmd = newCommand(1, mi.Err.Error())
		return
	} else {
		cmd = NewCommandData(0, "", mi.MergedOrder)
	}
	// this.Data["json"]=
}

//集合单详情
func (this *MainController) MergedOrderDetail() {
	id := this.GetString("ID")
	this.Data["json"] = pickupTekLib.GetMergedOrderDetailByID(id)
	this.ServeJson()
}

//通过合并包含的订单查找集合单
func (this *MainController) GetMergeOrderInfoByOrderID() {
	orderID := this.GetString("orderID")
	this.Data["json"] = NewCommandData(0, "", pickupTekLib.GetMergedOrderDetailByOrderID(orderID))
	this.ServeJson()
}

//返回所有集合单
func (this *MainController) MergedOrders() {
	this.Data["json"] = pickupTekLib.GetAllMergedOrders()
	this.ServeJson()
}

// 将集合单设置为完成状态
func (this *MainController) SetMergedOrderCompleted() {
	id := this.GetString("ID")
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	if err := pickupTekLib.SetMergedOrderCompleted(id); err != nil {
		cmd = newCommand(1, err.Error())
		return
	}
}
