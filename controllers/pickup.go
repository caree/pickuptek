package controllers

import (
	"fmt"
	// "github.com/astaxie/beego"
	// "github.com/ungerik/go-dry"
	// "path"
	// "github.com/BurntSushi/toml"
	// "io/ioutil"
	// "net/http"
	// "os"
	// "path/filepath"
	"pickupTek/pickupTekLib"
	"strconv"
	// "strings"
)

func (this *MainController) PickUpIndex() {
	id := this.GetString("ShelfID")
	fmt.Println("ShelfID: " + id)
	if len(id) <= 0 {
		id = "1"
		DebugSysF("无法获取参数 拣选墙编号")
		// fmt.Println("default :" + id)
	} else {
		i, err := strconv.Atoi(id)
		if err != nil {
			id = "1"
			// fmt.Println("parse error :" + id)
		} else {
			if i < G_conf.ShelfCount {
				id = strconv.Itoa(i + 1)
				// fmt.Println("+1 :" + id)
			} else {
				id = "1"
				// fmt.Println("newloop :" + id)
			}
		}
	}
	this.Data["ShelfID"] = id
	this.TplNames = "PickUpIndex.tpl"
}

func (this *MainController) PickingupOrdersDetailIndex() {
	ShelfID := this.GetString("ShelfID")
	this.Data["ShelfID"] = ShelfID
	this.TplNames = "PickingupOrdersDetailIndex.tpl"
}

func (this *MainController) ClearPosition() {
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	ID := this.GetString("ID")
	if len(ID) <= 0 {
		pickupTekLib.DebugSysF("无法获取参数 货位编号")
	}

	ShelfID := this.GetString("ShelfID")
	if len(ShelfID) <= 0 {
		DebugSysF("无法获取参数 拣选墙编号")
	}
	if err := pickupTekLib.ClearPosition(ShelfID, ID); err != nil {
		cmd = newCommand(1, err.Error())
		return
	}
}

//获取拣选墙的货位信息
func (this *MainController) GetPositionsNeedDetail() {
	ShelfID := this.GetString("ShelfID")
	this.Data["json"] = pickupTekLib.GetPositionsNeedDetail(ShelfID)
	this.ServeJson()
}

//未确认的订单项详情
func (this *MainController) NotConfirmedOrderItemList() {
	// this.Data["json"] = pickupTekLib.OrderItemsTemp
	this.Data["json"] = pickupTekLib.GetAllOrderItemsTemp()
	this.ServeJson()
}

// 统计尚在拣选中的订单的数量
func (this *MainController) GetUncompltedPickupOrdersCount() {
	cmd := newCommand(1, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	ShelfID := this.GetString("ShelfID")
	count := pickupTekLib.GetUncompltedPickupOrdersCount(ShelfID)
	cmd = NewCommandData(0, "", count)
}

// 提交拣选中扫描到的条码
func (this *MainController) SubmitPickupID() {
	// info := pickupTekLib.NewPickupInfo(0, nil, nil, "", "")
	ID := this.GetString("ID")
	ShelfID := this.GetString("ShelfID")
	info := pickupTekLib.PickUp(ID, ShelfID)
	defer func() {
		this.Data["json"] = info
		this.ServeJson()
	}()
	// if len(ID) <= 0 {
	// 	info = pickupTekLib.NewPickupInfo(6, nil, nil, "", "ID不能为空")
	// 	return
	// }
}
