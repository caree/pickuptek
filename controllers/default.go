package controllers

import (
	// "fmt"
	"github.com/astaxie/beego"
	// "github.com/ungerik/go-dry"
	// "path"
	"github.com/BurntSushi/toml"
	"io/ioutil"
	// "net/http"
	// "os"
	// "path/filepath"
	"pickupTek/pickupTekLib"
	// "strconv"
	// "strings"
)

var (
	G_conf           Config
	G_updatePrepared = false
	G_updaterport    = "12306"
)

type Command struct {
	Code    int
	Message string
	Data    interface{}
}

func NewCommandData(code int, msg string, data interface{}) Command {
	cmd := newCommand(code, msg)
	cmd.Data = data
	return cmd
}
func newCommand(code int, msg string) Command {
	return Command{
		Code:    code,
		Message: msg,
	}
}

//----------------------------------------------------------------------

func init() {
	DebugInfoF("应用 %s 运行中...", beego.AppName)
	initConfig()
	pickupTekLib.Init(G_conf.ShelfCount, G_conf.LocationCount)
	// G_updaterport = beego.AppConfig.String("updaterport")
	// DebugInfoF("升级助手的端口：%s", G_updaterport)
}
func initConfig() {

	confFile := "conf/sys.toml"
	if confData, err := ioutil.ReadFile(confFile); err != nil {
		DebugMustF("系统配置出错：%s", err.Error())
		return
	} else {
		if _, err := toml.Decode(string(confData), &G_conf); err != nil {
			DebugMustF("系统配置出错：%s", err.Error())
			return
		}
		DebugPrintList_Info(&G_conf)
	}
}

type MainController struct {
	beego.Controller
}

func (this *MainController) Index() {
	this.TplNames = "AppIndex.tpl"
}
