package controllers

import (
	// "fmt"
	// "github.com/astaxie/beego"
	// "github.com/ungerik/go-dry"
	// "path"
	// "github.com/BurntSushi/toml"
	// "io/ioutil"
	// "net/http"
	"os"
	// "path/filepath"
	"pickupTek/pickupTekLib"
	// "strconv"
	"strings"
)

func (this *MainController) AddProductIndex() {
	this.TplNames = "AddProductIndex.tpl"
}

func (this *MainController) ProductNameManagementIndex() {
	this.TplNames = "ProductNameManagementIndex.tpl"
}
func (this *MainController) ProductManagementIndex() {
	this.TplNames = "ProductManagementIndex.tpl"
}

func (this *MainController) AddProductBarcode() {
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	code := this.GetString("Code")
	name := this.GetString("Name")
	if err := pickupTekLib.AddProduct(code, name); err != nil {
		cmd = newCommand(1, err.Error())
		return
	}
}
func (this *MainController) ProductNameList() {
	this.Data["json"] = pickupTekLib.GetProductNamesDetails()
	this.ServeJson()
}
func (this *MainController) RemoveProductName() {
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	Name := strings.Trim(this.GetString("Name"), " ")
	if len(Name) <= 0 {
		cmd = newCommand(1, "商品名称不能为空")
		return
	}
	if err := pickupTekLib.RemoveProductName(Name); err != nil {
		cmd = newCommand(1, err.Error())
		return
	}

}
func (this *MainController) SearchProductByKeyword() {
	keyword := this.GetString("ID")
	this.Data["json"] = pickupTekLib.SearchProductByKeyword(keyword)
	this.ServeJson()
}
func (this *MainController) ProductList() {
	this.Data["json"] = pickupTekLib.GetAllProducts()
	this.ServeJson()
}
func (this *MainController) GetProductInfo() {
	ID := this.GetString("ID")
	this.Data["json"] = pickupTekLib.GetProductInfoByID(ID)
	this.ServeJson()
}
func (this *MainController) AddProduct() {
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	ID := this.GetString("ID")
	if len(ID) <= 0 {
		cmd = newCommand(1, "ID不能为空")
		return
	}
	name := this.GetString("name")
	if len(name) <= 0 {
		cmd = newCommand(1, "名称不能为空")
		return
	}
	if err := pickupTekLib.AddProduct(ID, name); err != nil {
		cmd = newCommand(1, "添加产品信息失败")
		return
	}
}

func (this *MainController) RemoveProduct() {
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	ID := this.GetString("ID")
	// Name := this.GetString("Name")
	if len(ID) <= 0 {
		cmd = newCommand(1, "ID不能为空")
		return
	}
	if err := pickupTekLib.RemoveProduct(ID); err != nil {
		cmd = newCommand(1, "移除产品信息失败")
		return
	}
}

func (this *MainController) UploadProductInfo() {
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()

	fromFile := "fileName"
	_, header, err := this.GetFile(fromFile)
	if err != nil {
		DebugSysF(err.Error())
	} else {
		if header != nil {
			// pickupTekLib.DebugInfo(header.Filename)
			// ext := path.Ext(header.Filename)
			fileName := header.Filename
			if err := this.SaveToFile(fromFile, "temp/"+fileName); err != nil {
				DebugSysF("保存文件出错：%s", err.Error())
				cmd = newCommand(1, "上传文件出错")
				return
			} else {
				if err := pickupTekLib.UpdateProductInfoFromFile(fileName); err != nil {
					DebugSysF(err.Error())
					cmd = newCommand(1, err.Error())
					return
				} else {
					//删除暂存文件
					if err := os.Remove("temp/" + fileName); err != nil {
						pickupTekLib.DebugMust("删除暂存文件时出错：" + err.Error())
					}
				}
			}
		} else {
			pickupTekLib.DebugInfo("没有文件上传")
			cmd = newCommand(1, "上传文件出错")
			return
		}
	}
}
func (this *MainController) AddProductBarcodeIndex() {
	this.Data["Barcode"] = this.GetString("ID")
	this.TplNames = "AddProductBarcodeIndex.tpl"
}
